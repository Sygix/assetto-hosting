import Head from 'next/head';

import { QueryLandingFromSlug } from '@/lib/graphql';

import Sections, { sectionTypeProps } from '@/components/sections';
import { HeroImage } from '@/components/sections/HeroImage';

import notFound from '@/app/[lang]/not-found';

export default async function Page({
  params: { lang, slug },
}: {
  params: { slug: string[]; lang: string };
}) {
  const { data } = await QueryLandingFromSlug(lang, slug);

  if (data.length <= 0) return notFound({ lang, slug });

  const pageID = data[0].id;
  const { content, heroImage, landing_footer, background_color } =
    data[0].attributes;

  const font = heroImage?.font;

  const googleFontUrl = `https://fonts.googleapis.com/css2?family=${font?.replace(
    / /g,
    '+'
  )}:wght@400;700&display=swap`;

  return (
    <>
      <Head>
        <link rel='stylesheet' href={googleFontUrl} />
      </Head>

      <div
        className={`-mt-12 min-h-screen w-full py-10 ${
          background_color ? `bg-[${background_color}]` : ''
        }`}
      >
        <HeroImage
          landingPageId={pageID}
          key={pageID}
          landing_footer={landing_footer}
        />
        <Sections
          sections={content as [sectionTypeProps]}
          pageID={pageID}
          pageType='landing'
        />
      </div>
    </>
  );
}
