import clsxm from '@/lib/clsxm';
import { QueryCategoryFromSlug } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';

import CarouselItem from '@/components/elements/carousel/CarouselItem';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import ButtonLink from '@/components/elements/links/ButtonLink';
import FormatPrice from '@/components/elements/texts/FormatPrice';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';
import Sections, { sectionTypeProps } from '@/components/sections';

import notFound from '@/app/[lang]/not-found';

const CategoryPage = async ({
  params: { lang, slug },
}: {
  params: { slug: string[]; lang: string };
}) => {
  const { data } = await QueryCategoryFromSlug(lang, slug);

  if (data.length <= 0) return notFound({ lang, slug });

  const pageID = data[0].id;
  const { title, description, price_text, btn_text, content, products } =
    data[0].attributes;

  return (
    <>
      <section className='flex w-full max-w-screen-md flex-col items-center justify-center gap-3 px-3 text-center md:gap-6 md:px-6 lg:px-12'>
        <h1 className='italic'>{title}</h1>
        <p className='text-carbon-700 dark:text-carbon-400'>{description}</p>
      </section>
      {products.data.length > 0 && (
        <section className='w-full max-w-screen-2xl px-3 md:px-6 lg:px-12'>
          <div
            className={clsxm(
              'dark:border-carbon-white overflow-hidden rounded-3xl border-2',
              products.data.length < 2
                ? ''
                : products.data.length < 3
                ? 'flex flex-col divide-y-2 xs:flex-row xs:divide-x-2 xs:divide-y-0'
                : 'flex flex-col divide-y-2 md:flex-row md:divide-x-2 md:divide-y-0'
            )}
          >
            {products.data.map((product) => (
              <div
                key={product.id}
                className='flex w-full flex-col xs:flex-row'
              >
                {products.data.length < 2 && (
                  <EmblaCarousel
                    className='w-full xs:w-1/3 lg:w-2/3'
                    containerClassName='w-full h-full'
                  >
                    {products.data[0].attributes.medias.data.map(
                      (media, index) => (
                        <CarouselItem
                          key={media.id}
                          index={index}
                          className='h-full w-full'
                        >
                          <NextImage
                            className='h-full w-full'
                            imgClassName='w-full h-full object-cover object-center'
                            useSkeleton
                            width={media.attributes.width}
                            height={media.attributes.height}
                            src={MediaUrl(media.attributes.url)}
                            alt={media.attributes.alternativeText ?? ''}
                            priority={index === 0}
                          />
                        </CarouselItem>
                      )
                    )}
                  </EmblaCarousel>
                )}

                <div
                  className={clsxm(
                    'flex w-full flex-col gap-3 p-3 xs:p-6',
                    products.data.length < 2
                      ? 'xs:w-2/3 lg:w-1/3'
                      : products.data.length < 3
                      ? 'lg:items-center lg:p-8'
                      : 'xs:items-center md:items-start lg:p-8'
                  )}
                >
                  <p className='h3'>{product.attributes.title}</p>
                  <p className='[&>.price]:text-5xl xs:[&>.price]:text-8xl'>
                    <FormatPrice
                      text={price_text}
                      prices={product.attributes.prices}
                    />
                  </p>
                  <hr
                    className={clsxm(
                      'rounded-full border-t-2',
                      products.data.length < 2
                        ? ''
                        : products.data.length < 3
                        ? 'lg:w-2/3'
                        : 'xs:w-2/3 md:w-full'
                    )}
                  />
                  <div className='prose prose-carbon max-w-full flex-1 dark:prose-invert dark:prose-dark md:prose-md'>
                    <RemoteMDX source={product.attributes.description} />
                  </div>
                  <div className='flex w-full justify-center'>
                    <ButtonLink
                      scroll={false}
                      className='px-6 font-semibold md:text-lg'
                      title={product.attributes.title}
                      href={includeLocaleLink(
                        `/checkout/${product.attributes.slug}`
                      )}
                    >
                      {btn_text}
                    </ButtonLink>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>
      )}
      <Sections
        sections={content as unknown as [sectionTypeProps]}
        pageID={pageID}
        pageType='category'
      />
    </>
  );
};

export default CategoryPage;
