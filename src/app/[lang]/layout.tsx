import { Metadata } from 'next';
import { Icon } from 'next/dist/lib/metadata/types/metadata-types';
import dynamic from 'next/dynamic';
import { Inter } from 'next/font/google';
import { cookies } from 'next/headers';
import { ReactNode } from 'react';

import '@/assets/styles/globals.css';

import {
  QueryCookiesSettings,
  Queryi18NLocales,
  QuerySettings,
  QueryStaticTexts,
} from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { PAYMENT_PROVIDER } from '@/lib/interfaces';
import { seo } from '@/lib/seo';

import Toasts from '@/components/elements/toaster/Toasts';
import Footer from '@/components/layout/Footer';
import Header from '@/components/layout/Header';
import ProvideSupport from '@/components/ProvideSupport';
import { ReferralProvider } from '@/components/ReferralProvider';
import MiniCookies from '@/components/sections/MiniCookies';
import ThemesProvider from '@/components/ThemesProvider';
import { ZustandProvider } from '@/components/ZustandProvider';

import { useCookieServer } from '@/store/cookieStore';
import { ServerState, useServer } from '@/store/serverStore';

const GoogleTag = dynamic(() => import('@/components/GoogleTag'), {
  ssr: false,
});

const inter = Inter({
  subsets: ['latin'],
  variable: '--font-inter',
  display: 'swap',
});

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    favicons,
    seo: defaultSeo,
    payment_provider,
    default_currency,
    currencies,
  } = await QuerySettings(lang);
  const iconList: Icon[] = [];
  useServer.setState({
    paymentProvider: payment_provider as PAYMENT_PROVIDER,
    currency: default_currency,
    currencies,
  });
  favicons.data.forEach((icon) => {
    return iconList.push(MediaUrl(icon.attributes.url));
  });

  const metadata = seo({
    ...defaultSeo,
    lang: lang,
    icons: iconList,
  });
  return metadata;
}

export default async function BaseLayout(props: {
  children: ReactNode;
  modal: ReactNode;
  params: { lang: string };
}) {
  const { google_tag_id, provide_support_script } = await QuerySettings(
    props.params.lang
  );
  const cookiesSetting = await QueryCookiesSettings(props.params.lang);
  const { translations } = await QueryStaticTexts(props.params.lang);
  const { i18NLocales } = await Queryi18NLocales();
  const currency = cookies().get('preferred_currency')?.value;

  const state: Partial<ServerState> = {
    locale: props.params.lang,
    locales: i18NLocales.data,
    translations: translations,
  };

  if (currency) state.currency = currency.toUpperCase();

  useCookieServer.setState({ cookiesSetting });
  useServer.setState(state);

  return (
    <html
      lang={props.params.lang ?? 'en'}
      className={`${inter.variable}`}
      suppressHydrationWarning
    >
      <body className='relative flex min-h-screen flex-col bg-white text-carbon-900 dark:bg-carbon-900'>
        <span className='absolute -z-10 h-screen w-full overflow-hidden'>
          <span
            className='absolute left-0 top-10 h-[1024px] w-[1024px] -translate-x-1/2 -translate-y-1/2 bg-contain bg-center bg-no-repeat lg:h-[2048px] lg:w-[2048px]'
            style={{ backgroundImage: 'url(/images/rond-violet.avif)' }}
          ></span>
          <span
            className='absolute bottom-1/2 right-0 h-[1024px] w-[1024px] translate-x-1/2 translate-y-1/2 bg-contain bg-center bg-no-repeat lg:h-[1500px] lg:w-[1500px]'
            style={{ backgroundImage: 'url(/images/rond-orange.avif)' }}
          ></span>
        </span>
        {provide_support_script && (
          <ProvideSupport script={provide_support_script} />
        )}
        {google_tag_id && (
          <GoogleTag
            gtmId={google_tag_id}
            cookieState={useCookieServer.getState()}
          >
            <MiniCookies />
          </GoogleTag>
        )}
        <ZustandProvider serverState={useServer.getState()} />
        <ReferralProvider />
        <ThemesProvider>
          <Header />
          <Toasts />
          <main className='flex flex-auto flex-col items-center gap-12 bg-white/40 py-12 text-carbon-900 dark:bg-carbon-900/70 dark:text-white md:gap-24'>
            {props.children}
          </main>
          <Footer />
        </ThemesProvider>
        {props.modal}
      </body>
    </html>
  );
}
