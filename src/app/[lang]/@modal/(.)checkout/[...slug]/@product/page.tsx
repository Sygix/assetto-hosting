import React from 'react';

import { QueryProductFromSlug, QuerySettings } from '@/lib/graphql';

import Modal from '@/components/elements/modal/Modal';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import CheckoutSteps from '@/components/layout/CheckoutSteps';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

async function CheckoutModal({
  params: { slug, lang },
}: {
  params: { slug: string[]; lang: string };
}) {
  const translations = useServer.getState().translations;

  const { paypal_client_id } = await QuerySettings(lang);
  const { data } = await QueryProductFromSlug(lang, slug);
  const product = data[0];

  return (
    <Modal dismissBack={true}>
      <div className='flex flex-col gap-3 divide-y-2 divide-carbon-300 overflow-hidden rounded-3xl bg-carbon-200 p-3 text-carbon-900 shadow-xl dark:divide-carbon-800 dark:border-2 dark:bg-carbon-900 dark:text-white dark:shadow-carbon-500/10 md:gap-6 md:p-6'>
        <h3 className='flex items-center gap-3'>
          <NextImage
            src='/images/card-dynamic-color.png'
            quality={100}
            width={30}
            height={30}
            alt='3d icon of a credit card'
            className='inline-block'
          />
          {translations.checkout?.subscribe_to} {product.attributes.title}
        </h3>

        {product.attributes.checkout_text && (
          <div className='prose prose-carbon max-w-full pt-3 dark:prose-invert dark:prose-dark md:prose-md prose-h1:italic md:pt-6'>
            <RemoteMDX source={product.attributes.checkout_text} />
          </div>
        )}

        <CheckoutSteps product={product} paypalClientId={paypal_client_id} />
      </div>
    </Modal>
  );
}

export default CheckoutModal;
