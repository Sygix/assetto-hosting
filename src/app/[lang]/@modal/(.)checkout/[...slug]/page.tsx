import React from 'react';

import { QueryPageFromSlug } from '@/lib/graphql';

import ButtonLink from '@/components/elements/links/ButtonLink';
import ModalWrapper from '@/components/layout/ModalWrapper';
import NextImage from '@/components/NextImage';
import Sections, { sectionTypeProps } from '@/components/sections';

import { useServer } from '@/store/serverStore';

async function CheckoutCompleteModal({
  params: { slug, lang },
}: {
  params: { slug: string[]; lang: string };
}) {
  const { data } = await QueryPageFromSlug(lang, slug);
  if (data.length <= 0) return null;

  const translations = useServer.getState().translations;

  const pageID = data[0].id;
  const { content } = data[0].attributes;

  return (
    <ModalWrapper routerRoute='/' className='max-w-fit'>
      <div className='flex w-fit flex-col gap-3 divide-y-2 divide-carbon-300 overflow-hidden rounded-3xl bg-carbon-200 p-3 text-carbon-900 shadow-xl dark:divide-carbon-800 dark:border-2 dark:bg-carbon-900 dark:text-white dark:shadow-carbon-500/10 md:gap-6 md:p-6'>
        <h3 className='flex items-center gap-3'>
          <NextImage
            src='/images/tick-dynamic-color.png'
            quality={100}
            width={30}
            height={30}
            alt='3d icon of a credit card'
            className='inline-block'
          />
          {translations.checkout?.subscribed ?? 'You are now subscribed'}
        </h3>

        <div className='flex w-fit flex-col gap-3 pt-3 md:gap-6 md:pt-6'>
          <Sections sections={content as [sectionTypeProps]} pageID={pageID} />
        </div>

        <div className='flex justify-center pt-3 md:pt-6'>
          <ButtonLink href='/' nextLinkProps={{ replace: true }}>
            {translations.btn?.back_to_home ?? 'back home'}
          </ButtonLink>
        </div>
      </div>
    </ModalWrapper>
  );
}

export default CheckoutCompleteModal;
