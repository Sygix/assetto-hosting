import Modal from '@/components/elements/modal/Modal';
import Cookies from '@/components/sections/Cookies';

async function CookieModal() {
  return (
    <Modal dismissBack={true} className='max-w-screen-lg'>
      <Cookies />
    </Modal>
  );
}

export default CookieModal;
