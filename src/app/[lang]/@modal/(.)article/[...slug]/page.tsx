import { format, parseISO } from 'date-fns';

import { QueryArticleFromSlug } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';

import Modal from '@/components/elements/modal/Modal';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';

async function ArticleModal({
  params: { slug, lang },
}: {
  params: { slug: string[]; lang: string };
}) {
  const { data } = await QueryArticleFromSlug(lang, slug);
  const { title, cover, author, publishedAt, content } = data[0].attributes;

  return (
    <Modal dismissBack={true} className='max-w-screen-2xl'>
      <div className='flex flex-col gap-3 overflow-hidden rounded-3xl bg-carbon-200 pb-3 shadow-xl dark:border-2 dark:bg-carbon-900 dark:shadow-carbon-500/10 md:gap-6 md:pb-6'>
        <div className='relative flex h-48 w-full items-center justify-center'>
          {cover.data && (
            <NextImage
              className='absolute h-full w-full after:absolute after:left-0 after:top-0 after:block after:h-full after:w-full after:bg-carbon-900/40'
              imgClassName='w-full h-full object-cover object-center'
              useSkeleton
              width={cover.data.attributes.width}
              height={cover.data.attributes.height}
              src={MediaUrl(cover.data.attributes.url)}
              alt={cover.data.attributes.alternativeText ?? ''}
            />
          )}
          <h1 className='relative z-10 text-center text-white'>{title}</h1>
        </div>

        <div className='flex w-full justify-center gap-1 font-semibold text-carbon-700 dark:text-carbon-400'>
          <address rel='author'>{author}</address>
          <span>-</span>
          <time dateTime={publishedAt}>
            {format(parseISO(publishedAt ?? '1970-01-01'), 'dd/MM/yyyy')}
          </time>
        </div>

        <div className='prose prose-carbon max-w-full px-3 dark:prose-invert dark:prose-dark md:prose-md prose-h1:italic md:px-6 '>
          <RemoteMDX source={content} />
        </div>
      </div>
    </Modal>
  );
}

export default ArticleModal;
