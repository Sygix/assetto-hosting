'use client';

import { ReactNode, useEffect, useRef, useState } from 'react';
import TagManager from 'react-gtm-module';

import { getFromLocalStorage } from '@/lib/helper';

import Modal from '@/components/elements/modal/Modal';

import { CookieState, useCookie } from '@/store/cookieStore';

const GoogleTag = ({
  gtmId,
  cookieState,
  children,
}: {
  gtmId: string;
  cookieState: CookieState;
  children: ReactNode;
}) => {
  const initialized = useRef(false);
  const [cookiesModalOpen, setCookiesModalOpen] = useState(false);

  useEffect(() => {
    if (!initialized.current && gtmId) {
      useCookie.setState({ ...cookieState });

      TagManager.initialize({
        gtmId,
      });

      const localConsent = getFromLocalStorage('consent');
      const parsedLocalConsent = localConsent ? JSON.parse(localConsent) : {};
      const { cookies: cookieList } = useCookie.getState().cookiesSetting;
      const consent = localConsent
        ? cookieList.every((cookie) =>
            Object.hasOwn(parsedLocalConsent, cookie.name)
          )
          ? parsedLocalConsent
          : Object.fromEntries(
              cookieList.map((cookie) => [cookie.name, cookie.default])
            )
        : Object.fromEntries(
            cookieList.map((cookie) => [cookie.name, cookie.default])
          );

      useCookie.getState().setConsent(consent);

      if (!localConsent) {
        setCookiesModalOpen(true);
      }

      initialized.current = true;
    }
  }, [cookieState, gtmId]);

  return (
    <>
      {gtmId && cookiesModalOpen && (
        <Modal
          dismissAction={() => setCookiesModalOpen(false)}
          className='left-0 top-full -translate-y-full translate-x-0'
          divClassName='bg-transparent'
          inWrapperClassName='w-fit max-w-full'
        >
          {children}
        </Modal>
      )}
    </>
  );
};

export default GoogleTag;
