export type HeroImageType = {
  image: {
    data: {
      attributes: {
        url: string;
        alternativeText: string;
      };
    };
  };
  title: string;
  subtitle: string;
  btn_text: string;
  has_button: boolean;
  image_size: number;
  full_screen_image: boolean;
  hightlight_in_title: string;
  button_link?: string;
  font?: string;
  use_custom_font_in_all_text?: boolean;
  use_custom_font_in_title?: boolean;
  image_footer?: string;
  content_centered?: boolean;
  title_size?: TitleSize;
  text_position?: TextPosition;
  text_overlay?: boolean;
};

export const enum TitleSize {
  M = 'm',
  L = 'l',
  XL = 'xl',
}

export const enum TextPosition {
  TOP = 'top',
  MIDDLE = 'middle',
  BOTTOM = 'bottom',
}
