import Link from 'next/link';

import clsxm from '@/lib/clsxm';
import { QueryHeroImageFromLanding } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';
import NextImage from '@/components/NextImage';
import { TextPosition, TitleSize } from '@/components/sections/HeroImage/types';

type Props = {
  landingPageId: number;
  landing_footer?: string;
};

export const HeroImage = async ({ landingPageId }: Props) => {
  const heroImage = await QueryHeroImageFromLanding(landingPageId);

  if (!heroImage) {
    return null;
  }

  const {
    title,
    subtitle,
    btn_text,
    has_button,
    hightlight_in_title,
    image,
    image_footer,
    full_screen_image,
    button_link,
    font,
    use_custom_font_in_all_text,
    title_size,
    use_custom_font_in_title,
    text_position,
    content_centered,
    text_overlay,
  } = heroImage;

  const imagePath = image?.data?.attributes?.url;
  const imageAlt = image?.data?.attributes?.alternativeText;
  const size = heroImage?.image_size ?? 300;

  return (
    <div
      className={clsxm(
        `relative flex h-[${size}px] -mt-12 w-full items-center overflow-hidden`,
        {
          'h-screen': full_screen_image,
        }
      )}
      style={{
        fontFamily: use_custom_font_in_all_text ? font : undefined,
      }}
    >
      <div
        className={clsxm(`md:p-23 absolute z-20 m-0 flex h-full w-full `, {
          'justify-center': !!content_centered,
          'items-start': text_position === TextPosition.TOP,
          'items-center': text_position === TextPosition.MIDDLE,
          'items-end': text_position === TextPosition.BOTTOM,
        })}
      >
        <div
          style={{
            backgroundImage: text_overlay
              ? 'linear-gradient(to bottom, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0.6) 90%, rgba(31, 41, 55, 0) 100%)'
              : '',
          }}
          className={clsxm('flex w-full bg-blend-darken', {
            ' justify-center': !!content_centered,
          })}
        >
          <div
            className={clsxm(
              'items flex w-full max-w-6xl flex-col space-y-2  px-6 pt-20 md:px-32 md:pb-12 md:pt-32',
              {
                'items-center': !!content_centered,
                'md:ml-24': !content_centered,
              }
            )}
          >
            <div
              style={{
                fontFamily: use_custom_font_in_title ? font : undefined,
              }}
              className={clsxm('flex items-end md:mx-0', {
                'text-6xl md:text-9xl': title_size === TitleSize.XL,
                'text-4xl md:text-7xl ': title_size === TitleSize.L,
                'text-2xl md:text-5xl': title_size === TitleSize.M,
              })}
            >
              <h1
                className={clsxm('font-black text-white', {
                  'text-center': !!content_centered,
                  'text-6xl md:text-9xl': title_size === TitleSize.XL,
                  'text-4xl md:text-7xl ': title_size === TitleSize.L,
                  'text-2xl md:text-5xl': title_size === TitleSize.M,
                })}
              >
                {title}
              </h1>
              <span className='-mb-0.5 font-black text-primary-400'>
                {hightlight_in_title}
              </span>
            </div>
            <span
              className={clsxm(' -mt-4 font-bold text-white ', {
                'py-4 text-center': !!content_centered,
                'text-xl md:text-3xl': title_size === TitleSize.XL,
                'text-lg md:text-2xl ': title_size === TitleSize.L,
                'text-sm md:text-2xl': title_size === TitleSize.M,
              })}
            >
              {subtitle}
            </span>
            {has_button && button_link ? (
              <Link href={button_link}>
                <Button
                  size='xl'
                  className='bg-primary-400 font-bold text-white'
                >
                  {btn_text}
                </Button>{' '}
              </Link>
            ) : null}
          </div>
        </div>
      </div>
      {image_footer ? (
        <div className='absolute bottom-16 left-0 z-20 px-6 md:px-20 '>
          <span className='text-2xl font-semibold text-white'>
            {image_footer}
          </span>
        </div>
      ) : null}
      <div className='z-10 h-full w-full bg-black bg-opacity-20' />
      {imagePath && imageAlt && (
        <NextImage
          alt={imageAlt}
          src={MediaUrl(imagePath)}
          imgClassName='w-full object-cover blur-sm z-1'
          fill
          sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
        />
      )}
    </div>
  );
};
