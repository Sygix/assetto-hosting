import { gql, QueryContentComponent, QueryLatestArticle } from '@/lib/graphql';
import { ArticleTag } from '@/lib/interfaces';

import ArticlesList from '@/components/elements/articles/ArticlesList';
import StatusText, { StatusEnum } from '@/components/elements/texts/StatusText';

import { useServer } from '@/store/serverStore';

const ComponentSectionsLatestArticles = gql`
  fragment sectionLatestArticles on ComponentSectionsLatestArticles {
    title
    status
    status_icon
    status_text
    page_size
    btn_text
    link_text
    article_tags {
      data {
        id
        attributes {
          name
        }
      }
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        status: StatusEnum;
        status_icon?: string;
        status_text?: string;
        page_size: number;
        btn_text: string;
        link_text: string;
        article_tags: { data: ArticleTag[] };
      }[];
    };
  };
};

const LatestArticles = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsLatestArticles,
    'sectionLatestArticles'
  );
  const {
    title,
    status,
    status_icon,
    status_text,
    page_size,
    btn_text,
    link_text,
    article_tags,
  } = content[props.index];
  const tag_ids = article_tags.data.map((tag) => tag.id);

  const { data: articles, meta } = await QueryLatestArticle(
    locale,
    1,
    page_size,
    tag_ids
  );

  return (
    <section className='flex w-full max-w-screen-xl flex-col items-center gap-3 px-3 md:gap-6 md:px-6 lg:px-12'>
      {title && <h2 className='italic'>{title}</h2>}
      {status_text && (
        <StatusText className='text-md' status={status} icon={status_icon}>
          <p className='font-semibold'>{status_text}</p>
        </StatusText>
      )}

      <ArticlesList
        articles={articles}
        pageSize={page_size}
        pageCount={meta.pagination?.pageCount}
        loadMoreText={btn_text}
        linkText={link_text}
        tag_ids={tag_ids}
      />
    </section>
  );
};

export default LatestArticles;
