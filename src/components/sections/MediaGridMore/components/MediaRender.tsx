import Link from 'next/link';

import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import DynamicIcon from '@/components/elements/DynamicIcon';
import NextImage from '@/components/NextImage';

type MediaRenderProps = {
  medias: {
    data: Media[];
  };
};
export const MediaRender = ({ medias }: MediaRenderProps) => {
  return medias.data.map((media) => {
    const {
      ext_video,
      thumbnail,
      slug,
      media: uploadFile,
      name,
    } = media.attributes;

    if (
      (ext_video || uploadFile.data?.attributes.mime.startsWith('video/')) &&
      thumbnail.data
    )
      return (
        <li key={media.id}>
          <Link
            title={name}
            href={includeLocaleLink(`/media/${slug}`)}
            scroll={false}
            className='relative'
          >
            <DynamicIcon
              icon='ph:play-fill'
              wrapperClassName='absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 drop-shadow-md'
              className='h-8 w-8 md:h-10 md:w-10'
            />
            <NextImage
              useSkeleton
              src={MediaUrl(thumbnail.data?.attributes.url ?? '')}
              width={thumbnail.data?.attributes.width ?? 0}
              height={thumbnail.data?.attributes.height ?? 0}
              alt={thumbnail.data?.attributes.alternativeText ?? ''}
              className='aspect-square w-full overflow-hidden rounded-3xl border-2 border-carbon-900 dark:border-transparent'
              imgClassName='object-cover object-center w-full h-full'
              sizes='100vw, (min-width: 475px) 50vw, (min-width: 1024px) 33vw, (min-width: 1536px) 25vw'
            />
          </Link>
        </li>
      );
    else if (uploadFile.data?.attributes.mime.startsWith('image/'))
      return (
        <li key={media.id}>
          <Link
            title={name}
            href={includeLocaleLink(`/media/${slug}`)}
            scroll={false}
          >
            <NextImage
              useSkeleton
              src={MediaUrl(uploadFile.data?.attributes.url ?? '')}
              width={uploadFile.data?.attributes.width ?? 0}
              height={uploadFile.data?.attributes.height ?? 0}
              alt={uploadFile.data?.attributes.alternativeText ?? ''}
              className='aspect-square w-full overflow-hidden rounded-3xl border-2 border-carbon-900 dark:border-transparent'
              imgClassName='object-cover object-center w-full h-full'
              sizes='100vw, (min-width: 475px) 50vw, (min-width: 1024px) 33vw, (min-width: 1536px) 25vw'
            />
          </Link>
        </li>
      );
    else if (
      uploadFile.data?.attributes.mime.startsWith('video/') &&
      !thumbnail.data
    )
      return (
        <li key={media.id}>
          <Link
            title={name}
            href={includeLocaleLink(`/media/${slug}`)}
            scroll={false}
          >
            <video
              autoPlay={true}
              loop={true}
              muted={true}
              width={uploadFile.data.attributes.width}
              height={uploadFile.data.attributes.height}
              className='aspect-square w-full rounded-3xl border-2 border-carbon-900 object-cover object-center dark:border-transparent'
            >
              <source
                src={MediaUrl(uploadFile.data.attributes.url)}
                type={uploadFile.data.attributes.mime}
              />
              <meta itemProp='name' content={uploadFile.data.attributes.name} />
              <meta
                itemProp='description'
                content={uploadFile.data.attributes.alternativeText}
              />
            </video>
          </Link>
        </li>
      );
    else return null;
  });
};
