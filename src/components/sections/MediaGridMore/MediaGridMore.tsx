import React from 'react';

import { gql, QueryContentComponent } from '@/lib/graphql';

import { MediaRender } from '@/components/sections/MediaGridMore/components/MediaRender';

import { useServer } from '@/store/serverStore';

// Definición del fragmento para la consulta
const ComponentSectionsMediaGridMore = gql`
  fragment sectionsMediaGridMore on ComponentSectionsMediaGridMore {
    title
    media_grid {
      medias {
        data {
          id
          attributes {
            name
            slug
            media {
              data {
                attributes {
                  name
                  alternativeText
                  caption
                  width
                  height
                  url
                  mime
                }
              }
            }
            thumbnail {
              data {
                attributes {
                  name
                  alternativeText
                  caption
                  width
                  height
                  url
                }
              }
            }
            ext_video
          }
        }
      }
    }
  }
`;

type MediaGridMoreProps = {
  pageID: number;
  index: number;
  pageType: string;
};

export const MediaGridMore = async ({
  pageID,
  index,
  pageType,
}: MediaGridMoreProps) => {
  const locale = useServer.getState().locale;

  const {
    data: {
      attributes: { content },
    },
  } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType, 'media'],
    ComponentSectionsMediaGridMore,
    'sectionsMediaGridMore'
  );

  const { title, media_grid } = content[index] || {};

  return (
    <section className='my-6 flex w-full max-w-screen-3xl flex-col items-center text-white'>
      {title && (
        <div className='flex w-5/6 justify-start'>
          <h2 className='text-center text-2xl font-normal uppercase md:text-4xl'>
            {title}
          </h2>
        </div>
      )}

      <div className='flex w-full list-none justify-center bg-gray-800 py-5'>
        <div className='grid w-full grid-cols-2  md:w-4/6'>
          <MediaRender medias={media_grid?.medias} />
        </div>
      </div>
    </section>
  );
};
