import React from 'react';

import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';

import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentQuoteDisplay = gql`
  fragment quoteDisplayFields on ComponentSectionsQuoteDisplay {
    quote {
      name
      quote
      image {
        data {
          attributes {
            url
            alternativeText
            width
            height
          }
        }
      }
    }
  }
`;

type QuoteDisplayProps = {
  pageID: number;
  index: number;
  pageType: string;
};

const QuoteDisplay = async ({ pageID, index, pageType }: QuoteDisplayProps) => {
  const locale = useServer.getState().locale;

  const {
    data: {
      attributes: { content },
    },
  } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType, 'quote_display'],
    ComponentQuoteDisplay,
    'quoteDisplayFields'
  );

  const { name, quote, image } = content[index]?.quote || {};

  return (
    <div className=' mx-auto mt-14 flex w-full max-w-5xl items-center justify-between rounded-md px-8 text-white shadow-lg'>
      <div className='relative flex flex-col space-y-4 bg-gray-800 p-6'>
        {quote && (
          <div className='text-sm leading-relaxed md:text-lg'>
            <RemoteMDX source={quote} />
          </div>
        )}

        {image?.data?.attributes && (
          <div className='absolute -right-4 -top-16 ml-6 flex items-center md:-top-20'>
            {name && (
              <h3 className='mb-1 mr-2 text-right text-sm font-medium md:text-lg'>
                {name}
              </h3>
            )}
            <div className='relative h-16 w-16 overflow-hidden rounded-full border-4 border-white shadow-lg md:h-24 md:w-24'>
              <NextImage
                src={MediaUrl(image.data.attributes.url)}
                alt={image.data.attributes.alternativeText ?? 'Author Image'}
                imgClassName='object-cover'
                fill
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default QuoteDisplay;
