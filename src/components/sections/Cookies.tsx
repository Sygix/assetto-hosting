import { Fragment } from 'react';

import CookiesAllButton from '@/components/elements/buttons/CookiesAllBtn';
import CookiesSwitch from '@/components/elements/buttons/CookiesSwitch';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';

import { useCookieServer } from '@/store/cookieStore';

const Cookies = async () => {
  const cookies = useCookieServer.getState().cookiesSetting;

  return (
    <div className='flex flex-col gap-3 overflow-hidden rounded-3xl bg-carbon-200 p-3 text-carbon-900 shadow-xl dark:border-2 dark:bg-carbon-900 dark:text-white dark:shadow-carbon-500/10 md:gap-6 md:p-6'>
      <div className='grid grid-cols-3 gap-3'>
        <div className='col-span-3 flex justify-end gap-3 md:gap-6'>
          <CookiesAllButton />
          <CookiesAllButton accept />
        </div>
        {cookies.cookies.map((cookie, index) => (
          <Fragment key={`${cookie.name}-${index}`}>
            <div className='col-span-2 flex flex-col gap-1'>
              <RemoteMDX source={cookie.description} />
            </div>
            <div className='flex items-center justify-center'>
              <CookiesSwitch cookie={cookie} />
            </div>
          </Fragment>
        ))}
      </div>
    </div>
  );
};

export default Cookies;
