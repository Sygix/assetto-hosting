import NextLink from 'next/link';

import clsxm from '@/lib/clsxm';
import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import FramerInfinite from '@/components/elements/carousel/FramerInfinite';
import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsDisplay = gql`
  fragment sectionDisplay on ComponentSectionsDisplay {
    title
    description
    scrolling_speed
    medias {
      data {
        id
        attributes {
          name
          slug
          media {
            data {
              attributes {
                name
                alternativeText
                caption
                width
                height
                url
                mime
              }
            }
          }

          thumbnail {
            data {
              attributes {
                name
                alternativeText
                caption
                width
                height
                url
              }
            }
          }

          ext_video
        }
      }
    }
    link {
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
      relationship
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        description?: string;
        scrolling_speed: number;
        link: LinkInterface;
        medias: {
          data: Media[];
        };
      }[];
    };
  };
};

const Display = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsDisplay,
    'sectionDisplay'
  );
  const { title, description, scrolling_speed, link, medias } =
    content[props.index];

  return (
    <section
      className={clsxm(
        'flex w-full max-w-screen-2xl flex-col gap-6 px-3 md:px-6 lg:flex-row lg:items-center lg:px-12',
        medias.data.length > 4 && 'lg:flex-col lg:items-start'
      )}
    >
      <div
        className={clsxm(
          'flex flex-col items-center gap-3 text-center md:flex-row md:justify-between md:text-left lg:h-fit lg:w-1/2 lg:flex-col lg:gap-6',
          medias.data.length > 4 && 'lg:w-full lg:flex-row'
        )}
      >
        {title && <h2 className='italic md:w-1/2 lg:w-full'>{title}</h2>}
        <div
          className={clsxm(
            'flex flex-col items-center gap-3 md:w-1/2 md:items-end lg:w-full lg:items-start lg:gap-6',
            medias.data.length > 4 && 'lg:items-end'
          )}
        >
          {description && (
            <p
              className={clsxm(
                'text-carbon-700 dark:text-carbon-400',
                title && 'md:text-right lg:text-left',
                medias.data.length > 4 && 'lg:text-right'
              )}
            >
              {description}
            </p>
          )}
          {link && (
            <Link
              title={link.name}
              href={includeLocaleLink(link.href)}
              openNewTab={link.open_new_tab}
              style={link.style}
              variant={link.variant}
              rel={link.relationship}
              icon={link.icon}
              direction={link.direction}
              size='lg'
              className=''
            >
              {link.name}
            </Link>
          )}
        </div>
      </div>

      {medias.data.length < 5 && (
        <div className='flex h-fit flex-wrap items-start justify-center lg:w-1/2'>
          {medias.data.map((media) => {
            const {
              ext_video,
              thumbnail,
              slug,
              media: uploadFile,
            } = media.attributes;

            if (
              ext_video ||
              (uploadFile.data?.attributes.mime.startsWith('video/') &&
                thumbnail.data)
            )
              return (
                <NextLink
                  title={media.attributes.name}
                  href={includeLocaleLink(`/media/${slug}`)}
                  key={media.id}
                  scroll={false}
                  className='h-fit w-1/2 p-3'
                >
                  <NextImage
                    useSkeleton
                    src={MediaUrl(thumbnail.data?.attributes.url ?? '')}
                    width={thumbnail.data?.attributes.width ?? 0}
                    height={thumbnail.data?.attributes.height ?? 0}
                    alt={thumbnail.data?.attributes.alternativeText ?? ''}
                    className='aspect-video w-full overflow-hidden rounded-xl shadow-xl'
                    imgClassName='object-cover object-center w-full h-full'
                  />
                </NextLink>
              );
            else if (uploadFile.data?.attributes.mime.startsWith('image/'))
              return (
                <NextLink
                  title={media.attributes.name}
                  href={includeLocaleLink(`/media/${slug}`)}
                  key={media.id}
                  scroll={false}
                  className='h-fit w-1/2 p-3'
                >
                  <NextImage
                    useSkeleton
                    src={MediaUrl(uploadFile.data?.attributes.url ?? '')}
                    width={uploadFile.data?.attributes.width ?? 0}
                    height={uploadFile.data?.attributes.height ?? 0}
                    alt={uploadFile.data?.attributes.alternativeText ?? ''}
                    className='aspect-video w-full overflow-hidden rounded-xl shadow-xl'
                    imgClassName='object-cover object-center w-full h-full'
                  />
                </NextLink>
              );
          })}
        </div>
      )}

      {/* 5 and more medias */}
      {medias.data.length > 4 && (
        <div className='w-full overflow-hidden'>
          <FramerInfinite speed={scrolling_speed}>
            {medias.data.map((media) => {
              const {
                ext_video,
                thumbnail,
                slug,
                media: uploadFile,
              } = media.attributes;

              if (
                ext_video ||
                (uploadFile.data?.attributes.mime.startsWith('video/') &&
                  thumbnail.data)
              )
                return (
                  <NextLink
                    title={media.attributes.name}
                    href={includeLocaleLink(`/media/${slug}`)}
                    key={media.id}
                    scroll={false}
                    className='h-fit w-60 shrink-0 grow-0 p-3 md:w-96'
                  >
                    <NextImage
                      useSkeleton
                      src={MediaUrl(thumbnail.data?.attributes.url ?? '')}
                      width={thumbnail.data?.attributes.width ?? 0}
                      height={thumbnail.data?.attributes.height ?? 0}
                      alt={thumbnail.data?.attributes.alternativeText ?? ''}
                      className='aspect-video w-full overflow-hidden rounded-xl'
                      imgClassName='object-cover object-center w-full h-full'
                    />
                  </NextLink>
                );
              else if (uploadFile.data?.attributes.mime.startsWith('image/'))
                return (
                  <NextLink
                    title={media.attributes.name}
                    href={includeLocaleLink(`/media/${slug}`)}
                    key={media.id}
                    scroll={false}
                    className='h-fit w-60 shrink-0 grow-0 p-3 md:w-96'
                  >
                    <NextImage
                      useSkeleton
                      src={MediaUrl(uploadFile.data?.attributes.url ?? '')}
                      width={uploadFile.data?.attributes.width ?? 0}
                      height={uploadFile.data?.attributes.height ?? 0}
                      alt={uploadFile.data?.attributes.alternativeText ?? ''}
                      className='aspect-video w-full overflow-hidden rounded-xl'
                      imgClassName='object-cover object-center w-full h-full'
                    />
                  </NextLink>
                );
            })}
          </FramerInfinite>
          <FramerInfinite speed={scrolling_speed} leftToRight={true}>
            {medias.data.map((media) => {
              const {
                ext_video,
                thumbnail,
                slug,
                media: uploadFile,
              } = media.attributes;

              if (
                ext_video ||
                (uploadFile.data?.attributes.mime.startsWith('video/') &&
                  thumbnail.data)
              )
                return (
                  <NextLink
                    title={media.attributes.name}
                    href={includeLocaleLink(`/media/${slug}`)}
                    key={media.id}
                    scroll={false}
                    className='h-fit w-60 shrink-0 grow-0 p-3 md:w-96'
                  >
                    <NextImage
                      useSkeleton
                      src={MediaUrl(thumbnail.data?.attributes.url ?? '')}
                      width={thumbnail.data?.attributes.width ?? 0}
                      height={thumbnail.data?.attributes.height ?? 0}
                      alt={thumbnail.data?.attributes.alternativeText ?? ''}
                      className='aspect-video w-full overflow-hidden rounded-xl'
                      imgClassName='object-cover object-center w-full h-full'
                    />
                  </NextLink>
                );
              else if (uploadFile.data?.attributes.mime.startsWith('image/'))
                return (
                  <NextLink
                    title={media.attributes.name}
                    href={includeLocaleLink(`/media/${slug}`)}
                    key={media.id}
                    scroll={false}
                    className='h-fit w-60 shrink-0 grow-0 p-3 md:w-96'
                  >
                    <NextImage
                      useSkeleton
                      src={MediaUrl(uploadFile.data?.attributes.url ?? '')}
                      width={uploadFile.data?.attributes.width ?? 0}
                      height={uploadFile.data?.attributes.height ?? 0}
                      alt={uploadFile.data?.attributes.alternativeText ?? ''}
                      className='aspect-video w-full overflow-hidden rounded-xl'
                      imgClassName='object-cover object-center w-full h-full'
                    />
                  </NextLink>
                );
            })}
          </FramerInfinite>
        </div>
      )}
    </section>
  );
};

export default Display;
