export type ScreenshotType = {
  id: number;
  title?: string;
  description?: string;
  text_color?: string;
  title_in_top?: boolean;
  image_size?: ScreenshotImageSizes;
  has_button?: boolean;
  btn_text?: string;
  btn_link?: string;
  btn_color?: string;
  uppercase_title?: boolean;
  image: {
    data: {
      attributes: {
        url: string;
        alternativeText?: string;
      };
    };
  };
  image_in_left?: boolean;
};

export const enum ScreenshotImageSizes {
  HALF = 'half',
  QUARTER = 'quarter',
  THIRD = 'third',
}
