import Link from 'next/link';
import React from 'react';

import clsxm from '@/lib/clsxm';
import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';
import {
  ScreenshotImageSizes,
  ScreenshotType,
} from '@/components/sections/ScreenshotDisplay/types';

import { useServer } from '@/store/serverStore';

const ComponentScreenshotDisplay = gql`
  fragment screenshotDisplayFields on ComponentSectionsScreenshotDisplay {
    ScreenShot {
      id
      title
      description
      text_color
      title_in_top
      image_size
      has_button
      btn_text
      btn_link
      btn_color
      uppercase_title
      image {
        data {
          attributes {
            url
            alternativeText
          }
        }
      }
      image_in_left
    }
  }
`;

type ScreenshotDisplayProps = {
  pageID: number;
  index: number;
  pageType: string;
};

export const ScreenshotDisplay = async ({
  pageID,
  index,
  pageType,
}: ScreenshotDisplayProps) => {
  const locale = useServer.getState().locale;

  const {
    data: {
      attributes: { content },
    },
  } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType],
    ComponentScreenshotDisplay,
    'screenshotDisplayFields'
  );

  const screenshots: ScreenshotType[] = content[index]?.ScreenShot || [];

  return (
    <div className='flex w-full flex-col  items-center space-y-6 p-6 pt-10  md:space-y-0'>
      {screenshots.map((screenshot, idx) => (
        <section
          key={screenshot.id}
          className={clsxm(
            `relative flex w-full max-w-6xl flex-col items-center justify-center gap-8 md:p-6 ${
              screenshot?.text_color
                ? `text-[${screenshot?.text_color}]`
                : 'text-white'
            }`,
            {
              'shadow-lg md:-mt-20': idx % 2 !== 0,
              'md:flex-row-reverse': !screenshot?.image_in_left,
              'flex-col-reverse md:flex-row': !!screenshot?.image_in_left,
            }
          )}
        >
          <div
            className={clsxm(
              `relative flex h-44 items-center  justify-center shadow-xl md:h-96`,
              {
                'w-3/4': !screenshot?.image_size,
                'w-1/2': screenshot?.image_size === ScreenshotImageSizes.HALF,
                'w-1/3': screenshot?.image_size === ScreenshotImageSizes.THIRD,
                'w-1/4':
                  screenshot?.image_size === ScreenshotImageSizes.QUARTER,
              }
            )}
          >
            <NextImage
              src={MediaUrl(screenshot.image.data.attributes.url)}
              alt={screenshot.image?.data?.attributes?.alternativeText ?? ''}
              imgClassName='rounded-sm shadow-xl object-contain'
              fill
            />
          </div>

          <div
            className={clsxm(
              'flex flex-grow flex-col items-center space-y-4 text-center md:items-start md:text-left',
              {
                'absolute right-32 top-0 mx-auto -mt-7 min-w-[600px] justify-end':
                  idx === 0 && !!screenshot?.title_in_top,
              }
            )}
          >
            <div className='flex flex-wrap items-center space-x-3'>
              <h2
                className={clsxm('font-semibold ', {
                  uppercase: screenshot.uppercase_title,
                  'text-2xl md:text-4xl':
                    screenshot.title && screenshot.title?.length > 10,
                  'text-4xl md:text-6xl':
                    screenshot.title && screenshot.title?.length < 10,
                })}
              >
                {screenshot?.title}
              </h2>
              {screenshot.has_button && screenshot?.btn_link && (
                <Link href={screenshot.btn_link}>
                  <Button
                    size='lg'
                    className='bg-primary-400 font-bold uppercase text-white'
                    disabled={!screenshot.has_button}
                  >
                    {screenshot?.btn_text}
                  </Button>
                </Link>
              )}
            </div>

            <RemoteMDX source={screenshot?.description ?? ''} />
          </div>
        </section>
      ))}
    </div>
  );
};
