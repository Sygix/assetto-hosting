import { gql, QueryContentComponent } from '@/lib/graphql';

import Typed from '@/components/elements/texts/Typed';

import { useServer } from '@/store/serverStore';

const ComponentSectionsTypedTitle = gql`
  fragment sectionsTypedTitle on ComponentSectionsTypedTitle {
    titles {
      text
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        titles: {
          text: string;
        }[];
      }[];
    };
  };
};

const TypedTitle = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsTypedTitle,
    'sectionsTypedTitle'
  );
  const { titles } = content[props.index];

  return (
    <h1 className='line-clamp-2 flex h-16 w-full items-center justify-center px-3 text-center italic xs:max-w-screen-xs md:h-20 md:max-w-screen-sm md:px-6 lg:px-12'>
      <Typed texts={titles.map((el) => el.text)}>{titles[0].text}</Typed>
    </h1>
  );
};

export default TypedTitle;
