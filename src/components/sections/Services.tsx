import Image from 'next/image';

import clsxm from '@/lib/clsxm';
import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { UploadFile } from '@/lib/interfaces';

import { useServer } from '@/store/serverStore';

const ComponentSectionsServices = gql`
  fragment sectionsServices on ComponentSectionsServices {
    title
    description
    services {
      id
      icon {
        data {
          attributes {
            alternativeText
            width
            height
            url
          }
        }
      }
      title
      description
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        description?: string;
        services: {
          id: number;
          icon: {
            data?: UploadFile;
          };
          title: string;
          description: string;
        }[];
      }[];
    };
  };
};

const Services = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsServices,
    'sectionsServices'
  );
  const { title, description, services } = content[props.index];

  return (
    <section className='flex w-full flex-col gap-6 px-3 md:px-6 lg:max-w-screen-2xl lg:px-12'>
      <div className='flex flex-col items-center gap-3 text-center md:flex-row md:justify-between md:text-left lg:gap-6'>
        {title && <h2 className='italic md:w-1/2'>{title}</h2>}
        {description && (
          <p
            className={clsxm(
              'text-carbon-700 dark:text-carbon-400 md:w-1/2',
              title && 'md:text-right'
            )}
          >
            {description}
          </p>
        )}
      </div>
      <div className='grid grid-cols-1 gap-3 xs:grid-cols-2 lg:grid-cols-4 lg:gap-6'>
        {services.map((service) => (
          <div
            key={service.id}
            className='flex flex-col gap-3 rounded-3xl border-2 border-carbon-900 p-3 dark:border-white md:p-6'
          >
            {service.icon.data && (
              <div className='flex w-full justify-center'>
                <Image
                  className='h-20 w-20 object-cover object-center hover:animate-spin'
                  width={service.icon.data.attributes.width}
                  height={service.icon.data.attributes.height}
                  src={MediaUrl(service.icon.data.attributes.url)}
                  alt={service.icon.data.attributes.alternativeText ?? ''}
                  sizes='50vw'
                />
              </div>
            )}
            <h3>{service.title}</h3>
            <p className='text-carbon-700 dark:text-carbon-400'>
              {service.description}
            </p>
          </div>
        ))}
      </div>
    </section>
  );
};

export default Services;
