import { includeLocaleLink } from '@/lib/helper';

import CookiesAllButton from '@/components/elements/buttons/CookiesAllBtn';
import ButtonLink from '@/components/elements/links/ButtonLink';

import { useServer } from '@/store/serverStore';

const MiniCookies = () => {
  const translations = useServer.getState().translations;

  return (
    <div className='flex flex-col gap-3 overflow-hidden rounded-3xl bg-carbon-200 p-3 text-carbon-900 shadow-xl dark:border-2 dark:bg-carbon-900 dark:text-white dark:shadow-carbon-500/10 md:gap-6 md:p-6'>
      <p>{translations.settings?.cookie_consent ?? 'Cookie consent :'}</p>
      <div className='flex flex-wrap gap-3'>
        <ButtonLink
          href={includeLocaleLink('/cookies')}
          rel='nofollow'
          className='bg-carbon-400 text-carbon-900 hover:bg-carbon-500 dark:bg-carbon-800 dark:text-white dark:hover:bg-carbon-700'
        >
          {translations.btn?.details ?? 'details'}
        </ButtonLink>
        <CookiesAllButton />
        <CookiesAllButton accept />
      </div>
    </div>
  );
};

export default MiniCookies;
