import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink } from '@/lib/helper';
import { LinkInterface } from '@/lib/interfaces';

import Link from '@/components/elements/links';
import Count from '@/components/elements/texts/Count';

import { useServer } from '@/store/serverStore';

const ComponentSectionsCtaNumbers = gql`
  fragment sectionsCtaNumbers on ComponentSectionsCtaNumbers {
    text_1
    text_2
    cta_btn {
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
      relationship
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        text_1: string;
        text_2?: string;
        cta_btn?: LinkInterface;
      }[];
    };
  };
};

const CtaNumbers = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsCtaNumbers,
    'sectionsCtaNumbers'
  );
  const { text_1, text_2, cta_btn } = content[props.index];

  const text1WithCounter = text_1.split(/\${([^}]*)}/).map((item, index) => {
    if (index % 2 === 0) return <span key={index}>{item}</span>;
    else if (!isNaN(Number.parseInt(item))) {
      return (
        <Count
          key={index}
          value={Number.parseInt(item)}
          className='text-primary-600'
        >
          {item}
        </Count>
      );
    } else
      return (
        <span key={index} className='text-primary-600'>
          {item}
        </span>
      );
  });

  const text2WithCounter = text_2?.split(/\${([^}]*)}/).map((item, index) => {
    if (index % 2 === 0) return <span key={index}>{item}</span>;
    else if (!isNaN(Number.parseInt(item))) {
      return (
        <Count
          key={index}
          value={Number.parseInt(item)}
          className='text-primary-600'
        >
          {item}
        </Count>
      );
    } else
      return (
        <span key={index} className='text-primary-600'>
          {item}
        </span>
      );
  });

  return (
    <section className='relative w-full overflow-hidden border-y-2 border-carbon-900 bg-white dark:border-white dark:bg-carbon-900'>
      <span
        className='absolute left-1/2 top-1/2 h-[1024px] w-[1024px] -translate-x-1/2 -translate-y-1/2 bg-contain bg-center bg-no-repeat'
        style={{ backgroundImage: 'url(/images/rond-violet.avif)' }}
      ></span>
      <div className='relative flex w-full justify-center bg-carbon-200/40 dark:bg-carbon-600/40'>
        <div className='flex w-full flex-col gap-6 p-3 md:p-6 lg:max-w-screen-2xl lg:px-12'>
          <div className='flex w-full flex-col items-center justify-between gap-3 xs:flex-row'>
            <h3 className='w-full text-center font-semibold'>
              {text1WithCounter}
            </h3>
            {text2WithCounter && (
              <h3 className='w-full text-center font-semibold'>
                {text2WithCounter}
              </h3>
            )}
          </div>
          {cta_btn && (
            <div className='flex w-full justify-center'>
              {cta_btn.style === 'button' && (
                <Link
                  title={cta_btn.name}
                  href={includeLocaleLink(cta_btn.href)}
                  openNewTab={cta_btn.open_new_tab}
                  style={cta_btn.style}
                  variant={cta_btn.variant}
                  rel={cta_btn.relationship}
                  icon={cta_btn.icon}
                  direction={cta_btn.direction}
                  className='bg-primary-600 text-lg font-semibold text-white md:text-2xl'
                  size='lg'
                >
                  {cta_btn.name}
                </Link>
              )}
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default CtaNumbers;
