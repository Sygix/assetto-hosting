import { MDXRemote } from 'next-mdx-remote/rsc';

import { gql, QueryContentComponent } from '@/lib/graphql';

import DynamicIcon from '@/components/elements/DynamicIcon';
import Accordion from '@/components/elements/texts/Accordion';

import { useServer } from '@/store/serverStore';

const ComponentSectionsFaq = gql`
  fragment sectionsFaq on ComponentSectionsFaq {
    small_text
    title
    items {
      id
      question
      response
      initial_open
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        small_text?: string;
        title: string;
        items: {
          id: number;
          question: string;
          response: string;
          initial_open: boolean;
        }[];
      }[];
    };
  };
};

const Faq = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsFaq,
    'sectionsFaq'
  );
  const { small_text, title, items } = content[props.index];

  return (
    <section className='flex w-full max-w-screen-sm flex-col items-center justify-center gap-3 px-3 md:gap-6 md:px-6 lg:px-12'>
      <div className='flex flex-col items-center'>
        {small_text && (
          <p className='font-semibold text-primary-600'>{small_text}</p>
        )}
        <h2 className='text-center italic'>{title}</h2>
      </div>
      {items.map((item) => (
        <Accordion
          key={item.id}
          initialOpen={item.initial_open}
          response={
            <span className='prose prose-sm prose-carbon mt-3 block dark:prose-invert'>
              <MDXRemote source={item.response} />
            </span>
          }
          className='flex w-full flex-col overflow-hidden rounded-2xl bg-carbon-300 p-6 hover:cursor-pointer dark:bg-carbon-800'
        >
          <span className='flex justify-between gap-3'>
            <h3 className='text-base text-carbon-800 dark:text-carbon-400'>
              {item.question}
            </h3>
            <DynamicIcon
              icon='octicon:chevron-right-12'
              className='h-6 w-6 rotate-90 rounded-full bg-carbon-400 p-1 dark:bg-carbon-700'
            />
          </span>
        </Accordion>
      ))}
    </section>
  );
};

export default Faq;
