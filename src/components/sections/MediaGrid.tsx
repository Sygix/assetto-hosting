import { gql, QueryContentComponent } from '@/lib/graphql';
import { Media } from '@/lib/interfaces';

import { MediaRender } from '@/components/sections/MediaGridMore/components/MediaRender';

import { useServer } from '@/store/serverStore';

const ComponentSectionsMediaGrid = gql`
  fragment sectionsMediaGrid on ComponentSectionsMediaGrid {
    medias {
      data {
        id
        attributes {
          name
          slug
          media {
            data {
              attributes {
                name
                alternativeText
                caption
                width
                height
                url
                mime
              }
            }
          }

          thumbnail {
            data {
              attributes {
                name
                alternativeText
                caption
                width
                height
                url
              }
            }
          }

          ext_video
        }
      }
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        medias: {
          data: Media[];
        };
      }[];
    };
  };
};

const MediaGrid = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType, 'media'],
    ComponentSectionsMediaGrid,
    'sectionsMediaGrid'
  );
  const { medias } = content[props.index];

  return (
    <section className='w-full max-w-screen-3xl px-3 md:px-6 lg:px-12'>
      <ul className='grid grid-cols-1 gap-3 xs:grid-cols-2 md:gap-6 lg:grid-cols-3 2xl:grid-cols-4'>
        <MediaRender medias={medias} />
      </ul>
    </section>
  );
};

export default MediaGrid;
