import React from 'react';

import { gql, QueryContentComponent } from '@/lib/graphql';

import { useServer } from '@/store/serverStore';

const ComponentBanner = gql`
  fragment bannerFields on ComponentSectionsBanner {
    text
    text_color
    border_color
    background_color
    has_borders
  }
`;

type BannerProps = {
  pageID: number;
  index: number;
  pageType: string;
};

export const Banner = async ({ pageID, index, pageType }: BannerProps) => {
  const locale = useServer.getState().locale;

  const {
    data: {
      attributes: { content },
    },
  } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType, 'banner'],
    ComponentBanner,
    'bannerFields'
  );

  const { text, text_color, border_color, background_color, has_borders } =
    content[index] || {};

  return (
    <div
      className={`flex w-full items-center justify-center py-4 text-4xl font-bold uppercase italic ${
        has_borders ? `border-b-4 border-t-4` : ''
      }`}
      style={{
        color: text_color || '#ffffff',
        backgroundColor: background_color || '#000000',
        borderColor: has_borders ? border_color || '#ffffff' : 'transparent',
      }}
    >
      {text}
    </div>
  );
};
