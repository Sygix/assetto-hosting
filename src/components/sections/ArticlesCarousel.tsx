import { format, parseISO } from 'date-fns';
import NextLink from 'next/link';

import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { Article, LinkInterface } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import CarouselBtn from '@/components/elements/carousel/CarouselBtn';
import CarouselItem from '@/components/elements/carousel/CarouselItem';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import DynamicIcon from '@/components/elements/DynamicIcon';
import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsArticlesCarousel = gql`
  fragment sectionArticlesCarousel on ComponentSectionsArticlesCarousel {
    title
    btn_text
    link {
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
      relationship
    }
    articles {
      data {
        id
        attributes {
          title
          slug
          short_description
          thumbnail {
            data {
              id
              attributes {
                name
                alternativeText
                caption
                width
                height
                url
                mime
              }
            }
          }
          author
          publishedAt
        }
      }
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        btn_text: string;
        link: LinkInterface;
        articles: {
          data: Article[];
        };
      }[];
    };
  };
};

const ArticlesCarousel = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsArticlesCarousel,
    'sectionArticlesCarousel'
  );
  const { title, btn_text, link, articles } = content[props.index];

  return (
    <section className='flex w-full max-w-screen-2xl flex-col items-center justify-center gap-3 px-3 md:gap-6 md:px-6 lg:px-12'>
      {title && <h2 className='text-center italic'>{title}</h2>}

      <div className='relative w-full px-12 md:px-20'>
        <EmblaCarousel
          containerClassName='ml-[calc(0.75rem*-1)] md:ml-[calc(1.5rem*-1)]'
          options={{
            loop: false,
            containScroll: 'trimSnaps',
            breakpoints: {
              '(min-width: 768px)': { align: 'start' },
            },
          }}
          otherChildrens={
            <>
              <CarouselBtn
                icon='ic:round-chevron-left'
                className='absolute left-0 top-1/2 z-10 -translate-y-1/2 text-4xl text-primary-600 md:text-6xl'
              />
              <CarouselBtn
                icon='ic:round-chevron-left'
                isNext
                className='absolute right-0 top-1/2 z-10 -translate-y-1/2 rotate-180 text-4xl text-primary-600 md:text-6xl'
              />
            </>
          }
        >
          {articles.data.map((article, index) => (
            <CarouselItem
              key={index}
              index={index}
              className='relative flex basis-full overflow-hidden pl-3 md:basis-1/2 md:pl-6 lg:basis-1/3 xl:basis-1/4'
            >
              <NextLink
                scroll={false}
                title={article.attributes.title}
                href={includeLocaleLink(`/article/${article.attributes.slug}`)}
              >
                <article
                  key={index}
                  className='flex h-full w-full flex-col gap-3 rounded-2xl bg-carbon-300 dark:bg-carbon-800 md:gap-6'
                >
                  <NextImage
                    className='h-fit w-full'
                    imgClassName='w-full aspect-video object-cover object-center rounded-2xl'
                    useSkeleton
                    width={article.attributes.thumbnail.data.attributes.width}
                    height={article.attributes.thumbnail.data.attributes.height}
                    src={MediaUrl(
                      article.attributes.thumbnail.data.attributes.url
                    )}
                    alt={
                      article.attributes.thumbnail.data.attributes
                        .alternativeText ?? ''
                    }
                  />
                  <div className='flex h-full flex-col items-center justify-start gap-3 p-2 pt-0 md:gap-6'>
                    <div className='flex w-full gap-1 font-semibold text-carbon-700 dark:text-carbon-400'>
                      <address rel='author'>
                        {article.attributes.author}
                      </address>
                      <span>-</span>
                      <time dateTime={article.attributes.publishedAt}>
                        {format(
                          parseISO(
                            article.attributes.publishedAt ?? '1970-01-01'
                          ),
                          'dd/MM/yyyy'
                        )}
                      </time>
                    </div>
                    <h3 className='line-clamp-2 w-full'>
                      {article.attributes.title}
                    </h3>
                    <div className='w-full flex-1'>
                      <p className='line-clamp-3 w-full'>
                        {article.attributes.short_description}
                      </p>
                    </div>
                    <Button
                      variant='dark'
                      className='w-fit bg-carbon-400 text-carbon-900 hover:bg-carbon-500 dark:bg-carbon-700 dark:text-white dark:hover:bg-carbon-600'
                    >
                      {btn_text}
                      <DynamicIcon
                        icon='material-symbols:chevron-right-rounded'
                        className='h-6 w-6 md:h-8 md:w-8'
                      />
                    </Button>
                  </div>
                </article>
              </NextLink>
            </CarouselItem>
          ))}
        </EmblaCarousel>
      </div>

      {link && (
        <Link
          href={includeLocaleLink(link.href)}
          openNewTab={link.open_new_tab}
          style={link.style}
          variant={link.variant}
          icon={link.icon}
          direction={link.direction}
          rel={link.relationship}
        >
          {link.name}
        </Link>
      )}
    </section>
  );
};

export default ArticlesCarousel;
