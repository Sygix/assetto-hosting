import { gql, QueryContentComponent } from '@/lib/graphql';

import GameRequestForm from '@/components/elements/forms/GameRequestForm';

import { useServer } from '@/store/serverStore';

const ComponentSectionsGameRequest = gql`
  fragment sectionsGameRequest on ComponentSectionsGameRequest {
    title
    btn_text
    email_placeholder
    game_placeholder
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title: string;
        btn_text: string;
        email_placeholder: string;
        game_placeholder: string;
      }[];
    };
  };
};

const GameRequest = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsGameRequest,
    'sectionsGameRequest'
  );
  const { title, btn_text, email_placeholder, game_placeholder } =
    content[props.index];

  return (
    <section className='flex w-full max-w-screen-sm flex-col items-center justify-center gap-3 px-3 md:gap-6 md:px-6 lg:px-12'>
      <h2 className='text-center italic'>{title}</h2>
      <GameRequestForm
        submitText={btn_text}
        placeholder={{ email: email_placeholder, game: game_placeholder }}
        className='flex w-full flex-col items-center gap-3'
      />
    </section>
  );
};

export default GameRequest;
