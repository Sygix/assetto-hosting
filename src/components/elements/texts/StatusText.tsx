import { ReactNode } from 'react';

import clsxm from '@/lib/clsxm';

import DynamicIcon from '@/components/elements/DynamicIcon';

export enum StatusEnum {
  operational = 'operational',
  maintenance = 'maintenance',
  degraded = 'degraded',
  down = 'down',
  unknown = 'unknown',
}

const StatusText = (props: {
  status: StatusEnum;
  className?: string;
  icon?: string;
  children?: ReactNode;
}) => {
  return (
    <div
      className={clsxm(
        'flex w-fit items-center gap-3 rounded-full border-2 px-4 py-2',
        props.status === 'operational'
          ? 'border-green-900 bg-green-200 text-green-900'
          : props.status === 'maintenance'
          ? 'border-yellow-900 bg-yellow-200 text-yellow-900'
          : props.status === 'degraded'
          ? 'text-orange-9900 border-orange-900 bg-orange-200'
          : props.status === 'down'
          ? 'border-red-900 bg-red-200 text-red-900'
          : 'border-carbon-900 bg-carbon-400 text-carbon-900',
        props.className
      )}
    >
      {props.icon && <DynamicIcon icon={props.icon} />}
      {props.children}
    </div>
  );
};

export default StatusText;
