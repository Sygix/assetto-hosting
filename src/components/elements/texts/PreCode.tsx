'use client';

import { ReactNode, useRef } from 'react';

import DynamicIcon from '@/components/elements/DynamicIcon';

import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

const PreCode = (props: { children?: ReactNode }) => {
  const preRef = useRef<HTMLPreElement | null>(null);
  const translations = useServer.getState().translations;
  const notify = useToaster((state) => state.notify);

  const copyToClipboard = () => {
    if (!preRef.current) return;

    const clipboard = navigator.clipboard;

    if (!clipboard) return;

    clipboard.writeText(preRef.current.innerText).then(() => {
      notify(0, <p>{translations.toaster.copied_to_clipboard}</p>);
    });
  };

  return (
    <div className='group/pre-code relative'>
      <button
        onClick={copyToClipboard}
        className='absolute right-4 top-4 rounded-full bg-carbon-700 p-1 text-xl text-carbon-100 opacity-0 transition-all duration-300 hover:text-carbon-50 group-hover/pre-code:opacity-100 dark:bg-carbon-950 dark:text-carbon-400 dark:hover:text-carbon-200'
      >
        <DynamicIcon icon='tabler:clipboard' />
      </button>

      <pre ref={preRef}>{props.children}</pre>
    </div>
  );
};

export default PreCode;
