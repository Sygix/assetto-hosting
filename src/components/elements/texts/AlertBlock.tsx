import { ReactNode } from 'react';

import clsxm from '@/lib/clsxm';

import DynamicIcon from '@/components/elements/DynamicIcon';

const AlertBlock = ({
  children,
  title,
  icon,
  className,
  ...rest
}: {
  children?: ReactNode;
  title?: string;
  icon?: string;
  className?: string;
}) => {
  return (
    <div
      className={clsxm(
        'not-prose flex w-fit flex-col gap-3 rounded-2xl bg-carbon-200 p-3 text-sm text-amber-800 dark:bg-carbon-800 dark:text-amber-500 md:text-base',
        className
      )}
      {...rest}
    >
      <div className='flex items-center gap-3 text-base'>
        {icon && <DynamicIcon icon={icon} className='h-6 w-6 md:h-8 md:w-8' />}
        {title && <h4 className='m-0'>{title}</h4>}
      </div>
      {children}
    </div>
  );
};

export default AlertBlock;
