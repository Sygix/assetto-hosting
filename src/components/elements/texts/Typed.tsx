'use client';

import { ReactNode, useEffect, useRef, useState } from 'react';
import TypedJS from 'typed.js';

const Typed = ({
  children,
  texts,
}: {
  children?: ReactNode;
  texts: string[];
}) => {
  // Create reference to store the DOM element containing the animation
  const el = useRef(null);
  // Create reference to store the Typed instance
  const typedRef = useRef<TypedJS | null>(null);
  // Check if client component is mounted
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    if (!isMounted) setTimeout(() => setIsMounted(true), 5000);
    else {
      typedRef.current = new TypedJS(el.current, {
        strings: texts,
        typeSpeed: 50,
        loop: true,
        loopCount: Infinity,
      });

      return () => {
        // Destroy Typed instance during cleanup to stop animation
        if (typedRef.current) typedRef.current.destroy();
      };
    }
  }, [isMounted, texts]);

  return (
    <>
      <span ref={el} translate='no'>
        {!isMounted && children}
      </span>
    </>
  );
};

export default Typed;
