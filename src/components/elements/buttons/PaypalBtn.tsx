'use client';

import { PayPalButtons, PayPalScriptProvider } from '@paypal/react-paypal-js';
import { useRouter } from 'next/navigation';

import { getFromCookies } from '@/lib/helper';
import { ProductPrice } from '@/lib/interfaces';

import { CheckoutFormType } from '@/components/elements/forms/CheckoutForm';

import { useServer } from '@/store/serverStore';

import createOrder from '@/actions/strapi/order';

const ButtonWrapper = ({
  planId,
  successPageSlug,
  formData,
}: {
  planId: string;
  successPageSlug: string;
  formData: CheckoutFormType;
}) => {
  const router = useRouter();
  const refCookie = getFromCookies('referral');

  return (
    <PayPalButtons
      createSubscription={(data, actions) => {
        return actions.subscription
          .create({
            plan_id: planId,
          })
          .then((orderId) => {
            return orderId;
          });
      }}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onApprove={(data: any, _) =>
        new Promise((resolve) => {
          const orderInput = {
            order_id: data.orderID,
            payment_source: data.paymentSource,
            subscription_id: data.subscriptionID,
            failed_payments_count: 0,
            delivery_email_address: formData.delivery_email_address,
            server_location: formData.server_location,
            referral_tag: refCookie,
          };
          createOrder(orderInput).then(() => {
            router.push(successPageSlug);
            resolve();
          });
        })
      }
      style={{
        shape: 'pill',
        color: 'gold',
        layout: 'vertical',
        tagline: false,
      }}
    />
  );
};

const intentType = ['subscription', 'capture'] as const;

const PaypalBtn = (props: {
  intentType: (typeof intentType)[number];
  prices: ProductPrice[];
  clientId: string;
  successPageSlug: string;
  formData: CheckoutFormType;
}) => {
  const userCurrency = useServer((state) => state.currency);
  const currentPrice = props.prices.find((el) => el.currency === userCurrency);

  return (
    <PayPalScriptProvider
      options={{
        clientId: props.clientId,
        components: 'buttons',
        intent: props.intentType,
        currency: userCurrency,
        vault: true,
      }}
    >
      {currentPrice?.paypal_plan_id && (
        <ButtonWrapper
          planId={currentPrice?.paypal_plan_id}
          successPageSlug={props.successPageSlug}
          formData={props.formData}
        />
      )}
    </PayPalScriptProvider>
  );
};

export default PaypalBtn;
