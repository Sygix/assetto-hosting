'use client';

import clsxm from '@/lib/clsxm';

import Button from '@/components/elements/buttons/Button';
import { useModalContext } from '@/components/elements/modal/Modal';

import { useCookie } from '@/store/cookieStore';
import { useServer } from '@/store/serverStore';

const CookiesAllButton = ({ accept = false }: { accept?: boolean }) => {
  const { setIsOpen } = useModalContext();
  const translations = useServer.getState().translations;
  const cookies = useCookie((state) => state.cookiesSetting);
  const setConsent = useCookie.getState().setConsent;

  const handleClick = () => {
    const consent = Object.fromEntries(
      cookies.cookies.map((cookie) => {
        return [
          cookie.name,
          cookie.mandatory ? 'granted' : accept ? 'granted' : 'denied',
        ];
      })
    );

    setConsent(consent, 'update');
    setIsOpen && setIsOpen(false);
  };

  return (
    <Button
      className={clsxm(
        !accept &&
          'bg-carbon-400 text-carbon-900 hover:bg-carbon-500 dark:bg-carbon-800 dark:text-white dark:hover:bg-carbon-700'
      )}
      onClick={handleClick}
    >
      {accept
        ? translations.btn?.accept_all ?? 'Accept all'
        : translations.btn?.reject_all ?? 'Reject all'}
    </Button>
  );
};

export default CookiesAllButton;
