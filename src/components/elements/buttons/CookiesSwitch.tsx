'use client';

import clsxm from '@/lib/clsxm';
import { CookiesSetting } from '@/lib/interfaces';

import Switch from '@/components/elements/buttons/Switch';

import { useCookie } from '@/store/cookieStore';

const CookiesSwitch = ({
  cookie,
}: {
  cookie: CookiesSetting['cookies'][0];
}) => {
  const setConsent = useCookie.getState().setConsent;
  const consent = useCookie((state) => state.consent);

  const handleCookieSwitch = (isOn: boolean) => {
    if (cookie.mandatory) return;
    consent[cookie.name] = isOn ? 'granted' : 'denied';

    setConsent(consent, 'update');
  };

  return (
    <Switch
      toggleIsOn={
        consent[cookie.name]
          ? consent[cookie.name] === 'granted'
          : cookie.default === 'granted'
      }
      toggle={handleCookieSwitch}
      className={clsxm(
        'bg-carbon-200 text-xl shadow-carbon-200-inner dark:bg-carbon-900 dark:shadow-carbon-900-inner md:text-2xl',
        cookie.mandatory ? 'cursor-not-allowed' : 'cursor-pointer'
      )}
      toggleClassName='bg-carbon-400 dark:bg-carbon-700'
      toggleIsOnClassName='bg-green-400 dark:bg-green-600'
      isDisabled={cookie.mandatory}
    />
  );
};

export default CookiesSwitch;
