'use client';

import { format, parseISO } from 'date-fns';
import React, { useState } from 'react';

import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { Article } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import ButtonLink from '@/components/elements/links/ButtonLink';
import NextImage from '@/components/NextImage';
import Skeleton from '@/components/Skeleton';

import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { loadMoreArticle } from '@/actions/strapi/load-more-articles';

const ArticlesList = ({
  articles,
  pageSize = 3,
  pageCount = 1,
  page = 1,
  loadMoreText = 'load more',
  linkText = 'read the info',
  tag_ids,
}: {
  articles: Article[];
  pageSize?: number;
  pageCount?: number;
  page?: number;
  loadMoreText?: string;
  linkText?: string;
  tag_ids?: number[];
}) => {
  const locale = useServer((state) => state.locale);
  const notify = useToaster((state) => state.notify);
  const translations = useServer.getState().translations;
  const [currentPage, setCurrentPage] = useState(page);
  const [listArticles, setListArticles] = useState<Article[]>(articles);
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadMore = () => {
    if (currentPage >= pageCount) return;
    setIsLoading(true);
    loadMoreArticle(locale, currentPage + 1, pageSize, tag_ids)
      .then((res) => {
        setCurrentPage((state) => state + 1);
        setListArticles((state) => [...state, ...res.data]);
      })
      .catch(() => {
        notify(
          1,
          <p>
            {translations.errors?.internal_server_error ??
              'Looks like something went wrong, please try again later'}
          </p>
        );
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <div className='flex flex-col items-center gap-6 md:gap-12'>
      <ul className='grid w-full auto-rows-fr gap-6 md:gap-12'>
        {listArticles.map((el) => (
          <li key={el.id} className='h-full w-full'>
            <article className='grid h-full w-full grid-cols-1 gap-3 xs:grid-cols-5 md:gap-6'>
              <NextImage
                className='h-fit w-full xs:col-span-2 xs:h-full lg:col-span-1'
                imgClassName='w-full h-full object-cover object-center rounded-lg'
                useSkeleton
                width={el.attributes.thumbnail.data.attributes.width}
                height={el.attributes.thumbnail.data.attributes.height}
                src={MediaUrl(el.attributes.thumbnail.data.attributes.url)}
                alt={
                  el.attributes.thumbnail.data.attributes.alternativeText ?? ''
                }
              />
              <div className='flex flex-col items-center gap-3 xs:col-span-3 xs:items-start md:gap-6 lg:col-span-4'>
                <div className='flex w-full gap-1 font-semibold text-carbon-700 dark:text-carbon-400'>
                  <address rel='author'>{el.attributes.author}</address>
                  <span>-</span>
                  <time dateTime={el.attributes.publishedAt}>
                    {format(
                      parseISO(el.attributes.publishedAt ?? '1970-01-01'),
                      'dd/MM/yyyy'
                    )}
                  </time>
                </div>
                <h3 className='w-full'>{el.attributes.title}</h3>
                <p className='line-clamp-3 w-full'>
                  {el.attributes.short_description}
                </p>
                <ButtonLink
                  title={el.attributes.title}
                  href={includeLocaleLink(`/article/${el.attributes.slug}`)}
                  scroll={false}
                  variant='dark'
                  className='w-fit'
                  icon='material-symbols:chevron-right-rounded'
                  iconClassName='w-6 h-6 md:w-8 md:h-8'
                >
                  {linkText}
                </ButtonLink>
              </div>
            </article>
          </li>
        ))}

        {/* Loading skeletton */}
        {isLoading && (
          <>
            {Array.from({ length: pageSize }).map((_, index) => (
              <li key={`skeleton-${index}`} className='hidden xs:block'>
                <article className='grid h-full w-full grid-cols-1 gap-3 xs:grid-cols-5 md:gap-6'>
                  <Skeleton className='h-full w-full rounded-lg xs:col-span-2 lg:col-span-1' />
                  <div className='flex flex-col items-center gap-3 xs:col-span-3 xs:items-start md:gap-6 lg:col-span-4'>
                    <Skeleton className='h-4 w-44 max-w-full rounded-full' />
                    <Skeleton className='h-6 w-64 max-w-full rounded-full' />
                    <div className='flex w-full flex-col gap-2'>
                      <Skeleton className='h-4 w-full rounded-full' />
                      <Skeleton className='h-4 w-4/5 rounded-full' />
                      <Skeleton className='h-4 w-3/5 rounded-full' />
                    </div>
                  </div>
                </article>
              </li>
            ))}
          </>
        )}
      </ul>
      {pageCount > 1 && (
        <Button
          disabled={currentPage >= pageCount}
          isLoading={isLoading}
          onClick={handleLoadMore}
          variant='dark'
          className='w-fit px-6 text-lg font-semibold md:text-xl'
        >
          {loadMoreText}
        </Button>
      )}
    </div>
  );
};

export default ArticlesList;
