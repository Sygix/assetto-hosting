'use client';

import {
  FieldErrors,
  FieldValues,
  get,
  Path,
  RegisterOptions,
  UseFormRegister,
} from 'react-hook-form';

import FormErrorMessage from '@/components/elements/forms/atoms/FormErrorMessage';
import Select, { SelectProps } from '@/components/elements/forms/atoms/Select';

export type FormSelectProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions<TFormValues, Path<TFormValues>>;
  register?: UseFormRegister<TFormValues>;
  errors?: FieldErrors<TFormValues>;
  errorClassName?: string;
} & Omit<SelectProps, 'name'>;

const FormSelect = <TFormValues extends Record<string, unknown>>({
  name,
  register,
  rules,
  errors,
  className,
  errorClassName,
  ...rest
}: FormSelectProps<TFormValues>): JSX.Element => {
  const errorMessage = (errors && get(errors, name)?.message) as string;

  return (
    <>
      <Select
        name={name}
        className={className}
        {...rest}
        {...(register && register(name, rules))}
      />
      {errorMessage && (
        <FormErrorMessage className={errorClassName}>
          {errorMessage}
        </FormErrorMessage>
      )}
    </>
  );
};

export default FormSelect;
