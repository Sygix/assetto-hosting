'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { InferType, object, ObjectSchema, ref, string } from 'yup';

import clsxm from '@/lib/clsxm';

import FormInput from '@/components/elements/forms/molecules/FormInput';
import FormSelect from '@/components/elements/forms/molecules/FormSelect';

import { useServer } from '@/store/serverStore';

let translations = useServer.getState().translations;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let schema: ObjectSchema<any> = object({
  delivery_email_address: string()
    .email(translations.forms?.invalid_email ?? 'invalid email')
    .required(translations.forms?.required_email ?? 'required email'),
  delivery_email_confirm: string().oneOf(
    [ref('delivery_email_address')],
    translations.forms?.email_not_match ?? "provided emails don't match"
  ),

  server_location: string().required(),
}).required();

export type CheckoutFormType = InferType<typeof schema>;

const CheckoutForm = ({
  className,
  submitCheckoutForm,
  server_locations,
}: {
  className?: string;
  submitCheckoutForm: (data: CheckoutFormType) => void;
  server_locations: string[];
}) => {
  translations = useServer.getState().translations;
  schema = object({
    delivery_email_address: string()
      .email(translations.forms?.invalid_email ?? 'invalid email')
      .required(translations.forms?.required_email ?? 'required email'),
    delivery_email_confirm: string().oneOf(
      [ref('delivery_email_address')],
      translations.forms?.email_not_match ?? "provided emails don't match"
    ),

    server_location: string()
      .oneOf(
        server_locations,
        translations.checkout?.server_location_one_of ??
          'server location must be one of the following:'
      )
      .required(),
  }).required();

  const {
    formState: { errors },
    handleSubmit,
    register,
  } = useForm<CheckoutFormType>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
  });

  return (
    <form
      id='checkout-form'
      onSubmit={handleSubmit(submitCheckoutForm)}
      className={clsxm(className)}
    >
      <span className='w-full'>
        <label
          className='pb-1 text-carbon-700 dark:text-carbon-400'
          htmlFor='delivery_email_address'
        >
          {translations.checkout?.delivery_email_label ?? 'Delivery email'}
        </label>
        <FormInput
          name='delivery_email_address'
          label='delivery_email_address'
          placeholder={translations.forms?.placeholder_email}
          className='w-full rounded-full border-2 border-carbon-300 bg-transparent px-3 py-2 text-sm placeholder:text-carbon-700 dark:border-carbon-800 dark:placeholder:text-carbon-400 md:text-base'
          register={register}
          errors={errors}
          errorClassName='text-red-600 text-sm'
        />
        <label
          className='pb-1 text-carbon-700 dark:text-carbon-400'
          htmlFor='delivery_email_confirm'
        >
          {translations.checkout?.confirm_email_label ?? 'Confirm email'}
        </label>
        <FormInput
          name='delivery_email_confirm'
          label='delivery_email_confirm'
          className='w-full rounded-full border-2 border-carbon-300 bg-transparent px-3 py-2 text-sm placeholder:text-carbon-700 dark:border-carbon-800 dark:placeholder:text-carbon-400 md:text-base'
          register={register}
          errors={errors}
          errorClassName='text-red-600 text-sm'
        />
      </span>

      <span className='w-full'>
        <label
          className='pb-1 text-carbon-700 dark:text-carbon-400'
          htmlFor='server_location'
        >
          {translations.checkout?.server_location_label ?? 'Server location'}
        </label>
        <FormSelect
          name='server_location'
          label='server_location'
          className='w-full rounded-full border-2 border-carbon-300 bg-transparent px-3 py-2 text-sm placeholder:text-carbon-700 dark:border-carbon-800 dark:placeholder:text-carbon-400 md:text-base dark:[&>*]:bg-carbon-800'
          register={register}
          errors={errors}
          errorClassName='text-red-600 text-sm'
          options={[
            translations.checkout?.server_location_default ??
              '--Select a location--',
            ...server_locations,
          ]}
          defaultValue={
            translations.checkout?.server_location_default ??
            '--Select a location--'
          }
        />
      </span>
    </form>
  );
};

export default CheckoutForm;
