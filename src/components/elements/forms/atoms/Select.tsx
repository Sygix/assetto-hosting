import { forwardRef } from 'react';

import clsxm from '@/lib/clsxm';

export type SelectProps = {
  label: string;
  options: string[];
} & React.ComponentPropsWithRef<'select'>;

const Select = forwardRef<HTMLSelectElement, SelectProps>(
  ({ label, className, options, ...rest }, ref) => {
    return (
      <select
        ref={ref}
        aria-label={label}
        className={clsxm('form-select', className)}
        {...rest}
      >
        {options.map((value, index) => (
          <option key={value + '-' + index} value={value}>
            {value}
          </option>
        ))}
      </select>
    );
  }
);

export default Select;
