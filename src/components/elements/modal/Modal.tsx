'use client';

import dynamic from 'next/dynamic';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import {
  createContext,
  MouseEventHandler,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';

import clsxm from '@/lib/clsxm';

const AnimatePresence = dynamic(() =>
  import('framer-motion').then((mod) => mod.AnimatePresence)
);
const MotionDiv = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.div)
);

const ModalContext = createContext<{
  isOpen?: boolean;
  setIsOpen?: (isOpen: boolean) => void;
}>({});

export const useModalContext = () => {
  const context = useContext(ModalContext);
  if (!context) {
    throw new Error('useModalContext must be used within a Modal');
  }
  return context;
};

type ModalProps = {
  children: React.ReactNode;
  className?: string;
  divClassName?: string;
  overlayClassName?: string;
  inWrapperClassName?: string;
  dismissBack?: boolean;
  dismissAction?: () => void;
  openModal?: boolean;
};

export default function Modal({
  children,
  className,
  divClassName,
  overlayClassName,
  inWrapperClassName,
  dismissBack = false,
  dismissAction,
  openModal = true,
}: ModalProps) {
  const overlay = useRef(null);
  const wrapper = useRef(null);
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(openModal);
  const [openPath, setOpenPath] = useState<string | null>();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const overlayVariants = {
    open: {
      opacity: 1,
    },
    closed: {
      opacity: 0,
    },
  };

  const wrapperVariants = {
    open: {
      opacity: 1,
      scale: 1,
      transition: {
        type: 'spring',
        duration: 0.3,
      },
    },
    closed: {
      opacity: 0,
      scale: 0,
      transition: {
        type: 'spring',
        duration: 0.3,
      },
    },
  };

  const onDismiss = useCallback(() => {
    if (openPath !== `${pathname}?${searchParams}`) return;
    dismissBack && router.back();
    dismissAction !== undefined && dismissAction();
  }, [openPath, pathname, searchParams, dismissBack, router, dismissAction]);

  const onClick: MouseEventHandler = useCallback(
    (e) => {
      if (e.target === overlay.current || e.target === wrapper.current) {
        if (isOpen) setIsOpen(false);
      }
    },
    [isOpen, overlay, wrapper]
  );

  const onKeyDown = useCallback((e: KeyboardEvent) => {
    if (e.key === 'Escape') setIsOpen(false);
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', onKeyDown);
    return () => document.removeEventListener('keydown', onKeyDown);
  }, [onKeyDown]);

  useEffect(() => {
    !openPath && setOpenPath(`${pathname}?${searchParams}`);
    if (openPath && openPath !== `${pathname}?${searchParams}`)
      setIsOpen(false);
  }, [openPath, pathname, searchParams]);

  return (
    <ModalContext.Provider value={{ isOpen, setIsOpen }}>
      <AnimatePresence onExitComplete={onDismiss}>
        {isOpen && (
          <MotionDiv
            initial={overlayVariants.closed}
            animate={overlayVariants.open}
            exit={overlayVariants.closed}
            className={clsxm(
              'fixed inset-0 z-50 mx-auto bg-carbon-900/60',
              divClassName
            )}
          >
            <div
              ref={overlay}
              onClick={onClick}
              className={clsxm('h-full w-full', overlayClassName)}
            >
              <div
                ref={wrapper}
                className={clsxm(
                  'absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2',
                  'max-h-[100dvh] w-full max-w-screen-lg sm:w-10/12',
                  'no-scrollbar overflow-y-scroll p-6',
                  className
                )}
              >
                <MotionDiv
                  initial={wrapperVariants.closed}
                  animate={wrapperVariants.open}
                  exit={wrapperVariants.closed}
                  className={clsxm('h-full w-full', inWrapperClassName)}
                >
                  {children}
                </MotionDiv>
              </div>
            </div>
          </MotionDiv>
        )}
      </AnimatePresence>
    </ModalContext.Provider>
  );
}
