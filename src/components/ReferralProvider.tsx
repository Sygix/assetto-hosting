'use client';

import { useSearchParams } from 'next/navigation';
import { useEffect, useRef } from 'react';

import { MutationReferralIncreaseHits } from '@/lib/graphql';
import { getFromCookies, setCookie } from '@/lib/helper';

import { useCookie } from '@/store/cookieStore';

export const ReferralProvider = () => {
  const searchParams = useSearchParams();
  const increased = useRef(false);
  const ref = searchParams.get('ref');
  const refCookie = getFromCookies('referral');
  const consent = useCookie((state) => state.consent);

  useEffect(() => {
    if (!consent) return;
    // Increase referral hits only once and not if referral cookie is present
    if (ref && !refCookie && !increased.current) {
      MutationReferralIncreaseHits(ref);
      increased.current = true;
    }

    // Set referral cookie if analytics_storage is allowed
    if (ref && consent.analytics_storage === 'granted') {
      setCookie('referral', ref);
    }
  }, [consent, ref, refCookie]);

  return null;
};
