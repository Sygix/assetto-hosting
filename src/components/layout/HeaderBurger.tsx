'use client';

import dynamic from 'next/dynamic';
import { useEffect, useRef, useState } from 'react';

import { includeLocaleLink } from '@/lib/helper';
import { HeaderItem } from '@/lib/interfaces';

import ThemeSwitch from '@/components/elements/buttons/ThemeSwitch';
import DynamicIcon from '@/components/elements/DynamicIcon';
import Link from '@/components/elements/links';
import SettingsButton from '@/components/layout/SettingsButton';
import NextImage from '@/components/NextImage';

const MotionNav = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.nav)
);

const MotionButton = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.button)
);

const MotionUl = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.ul)
);

const MotionLi = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.li)
);

const HeaderBurger = ({
  items,
  className,
}: {
  items: HeaderItem[];
  className?: string;
}) => {
  const navRef = useRef<HTMLSpanElement>(null);
  const ulRef = useRef<HTMLSpanElement>(null);
  const [isOpen, setIsOpen] = useState(false);
  const [subIsOpen, setSubIsOpen] = useState(false);

  const ulVariants = {
    open: {
      display: 'flex',
      opacity: 1,
      transition: {
        when: 'beforeChildren',
        delayChildren: 0.1,
        staggerChildren: 0.05,
      },
    },
    closed: {
      display: 'none',
      opacity: 0,
      transition: {
        when: 'afterChildren',
      },
    },
  };

  const itemVariants = {
    open: {
      opacity: 1,
      y: 0,
      transition: {
        type: 'spring',
        stiffness: 300,
        damping: 24,
      },
    },
    closed: {
      opacity: 0,
      y: 20,
      transition: {
        duration: 0.2,
      },
    },
  };

  useEffect(() => {
    if (!isOpen) return;
    function handleClick({ target }: MouseEvent) {
      if (
        navRef.current &&
        ulRef.current &&
        !navRef.current.contains(target as Node) &&
        !ulRef.current.contains(target as Node)
      ) {
        setIsOpen(false);
      }
    }
    window.addEventListener('click', handleClick);
    return () => window.removeEventListener('click', handleClick);
  }, [isOpen]);

  return (
    <span ref={navRef} className={className}>
      <MotionNav initial={false} animate={isOpen ? 'open' : 'closed'}>
        <MotionButton
          onClick={() => setIsOpen(!isOpen)}
          className='h-8 w-8 p-0'
          aria-label='menu'
        >
          <DynamicIcon
            icon='material-symbols:menu-rounded'
            className='h-full w-full text-carbon-900 dark:text-white'
          />
        </MotionButton>
        <span ref={ulRef}>
          <MotionUl
            variants={ulVariants}
            className='absolute left-0 top-0 -z-10 hidden h-screen max-h-screen w-full flex-col items-center gap-3 bg-carbon-200 pb-10 pt-20 dark:bg-carbon-900'
          >
            {items.map((item) => (
              <MotionLi
                variants={itemVariants}
                key={item.id}
                className='mx-3 flex w-full flex-col items-center'
              >
                <span className='inline-flex items-center gap-1'>
                  <Link
                    title={item.link.name}
                    href={includeLocaleLink(item.link.href)}
                    style={item.link.style}
                    icon={item.link.icon}
                    variant={item.link.variant}
                    rel={item.link.relationship}
                    openNewTab={item.link.open_new_tab}
                    className='flex w-full justify-center font-bold dark:font-bold'
                    onClick={() => setIsOpen((state) => !state)}
                  >
                    {item.link.name}
                  </Link>
                  {item.sublinks.length > 0 && (
                    <MotionButton
                      onClick={() => setSubIsOpen((state) => !state)}
                    >
                      <DynamicIcon
                        icon='material-symbols:keyboard-arrow-down-rounded'
                        className='h-8 w-8 text-carbon-900 dark:text-white'
                      />
                    </MotionButton>
                  )}
                </span>

                {item.sublinks.length > 0 && subIsOpen && (
                  <ul className='item-center flex flex-col gap-3 pb-6 pt-2 text-sm font-normal'>
                    {item.sublinks.map((subItem) => (
                      <li key={subItem.id} className='flex justify-center'>
                        <Link
                          title={subItem.name}
                          href={includeLocaleLink(subItem.href)}
                          style={subItem.style}
                          icon={subItem.icon}
                          openNewTab={item.link.open_new_tab}
                          variant={subItem.variant}
                          rel={subItem.relationship}
                          className='font-semibold dark:font-semibold'
                          onClick={() => setIsOpen((state) => !state)}
                        >
                          {subItem.name}
                        </Link>
                      </li>
                    ))}
                  </ul>
                )}
              </MotionLi>
            ))}

            <MotionLi
              variants={itemVariants}
              className='mx-3 flex w-full justify-between p-3'
            >
              <div className='h-10 w-20'>
                <ThemeSwitch />
              </div>
              <SettingsButton className='flex h-10 items-center gap-1 rounded-full bg-carbon-200 p-1 px-2 shadow-carbon-200-inner dark:bg-carbon-900 dark:shadow-carbon-900-inner'>
                <NextImage
                  src='/images/region.png'
                  quality={100}
                  width={30}
                  height={30}
                  alt='3d settings icon'
                />
              </SettingsButton>
            </MotionLi>
          </MotionUl>
        </span>
      </MotionNav>
    </span>
  );
};

export default HeaderBurger;
