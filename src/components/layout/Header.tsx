import Image from 'next/image';
import * as React from 'react';

import { QueryMenus } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';

import ThemeSwitch from '@/components/elements/buttons/ThemeSwitch';
import DynamicIcon from '@/components/elements/DynamicIcon';
import Link from '@/components/elements/links';
import UnstyledLink from '@/components/elements/links/UnstyledLink';
import HeaderBurger from '@/components/layout/HeaderBurger';
import HeaderItem from '@/components/layout/HeaderItem';
import SettingsButton from '@/components/layout/SettingsButton';

import { useServer } from '@/store/serverStore';

export default async function Header() {
  const locale = useServer.getState().locale;
  const { data } = await QueryMenus(locale);
  const { header } = data.attributes;

  return (
    <header className='sticky top-0 z-40 border-b-2 border-carbon-900 bg-carbon-200 font-bold text-carbon-900 dark:border-0  dark:bg-carbon-900 dark:text-white'>
      <div className='layout flex w-full flex-row items-center justify-between gap-3 p-3 text-base md:text-sm lg:gap-6 lg:px-12 xl:text-base'>
        <UnstyledLink
          title='home'
          href={includeLocaleLink(header.logo_link)}
          className='shrink-1 flex'
          aria-label='home page'
        >
          <Image
            src={MediaUrl(header.logo.data.attributes.url)}
            priority
            quality={100}
            width={header.logo.data.attributes.width}
            height={header.logo.data.attributes.height}
            alt={header.logo.data.attributes.alternativeText ?? ''}
            className='h-10 w-full object-contain object-left'
            sizes='50vw'
          />
        </UnstyledLink>

        <HeaderBurger
          items={header.items}
          className='flex justify-center md:hidden'
        />

        {/* Desktop links */}
        <ul className='shrink-1 hidden gap-3 md:flex md:items-center lg:gap-6'>
          {header.items.map((item) => (
            <HeaderItem
              key={item.id}
              sublinks={item.sublinks}
              className='flex flex-row items-center gap-1'
            >
              <Link
                title={item.link.name}
                href={includeLocaleLink(item.link.href)}
                style={item.link.style}
                icon={item.link.icon}
                openNewTab={item.link.open_new_tab}
                variant={item.link.variant}
                rel={item.link.relationship}
                className='font-bold dark:font-bold'
              >
                {item.link.name}
              </Link>
              {item.sublinks.length > 0 && (
                <span className='h-8 w-8'>
                  <DynamicIcon
                    icon='material-symbols:keyboard-arrow-down-rounded'
                    className='h-full w-full text-carbon-900 dark:text-white'
                  />
                </span>
              )}
            </HeaderItem>
          ))}
        </ul>

        <div className='hidden shrink-0 md:flex lg:gap-3'>
          {/* This will be the theme and lang/currency buttons */}
          <div className='flex h-10 w-20 items-center'>
            <ThemeSwitch className='h-full' />
          </div>
          <SettingsButton className='flex h-10 items-center gap-1 rounded-full bg-carbon-200 p-1 px-2 shadow-carbon-200-inner dark:bg-carbon-900 dark:shadow-carbon-900-inner'>
            <Image
              src='/images/region.png'
              quality={100}
              width={30}
              height={30}
              alt='3d settings icon'
            />
          </SettingsButton>
        </div>
      </div>
    </header>
  );
}
