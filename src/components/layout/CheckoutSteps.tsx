'use client';

import { useState } from 'react';

import { Product } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import PaypalBtn from '@/components/elements/buttons/PaypalBtn';
import CheckoutForm, {
  CheckoutFormType,
} from '@/components/elements/forms/CheckoutForm';
import PreviousLink from '@/components/elements/links/PreviousLink';
import FormatPrice from '@/components/elements/texts/FormatPrice';
import PriceDetails from '@/components/elements/texts/PriceDetails';

import { useServer } from '@/store/serverStore';

const CheckoutSteps = (props: {
  product: Product;
  paypalClientId?: string;
}) => {
  const [step, setStep] = useState(1);
  const [formData, setFormData] = useState<CheckoutFormType | null>(null);
  const translations = useServer.getState().translations;

  const handleCheckoutForm = (data: CheckoutFormType) => {
    setFormData(data);
    setStep(2);
  };

  return (
    <>
      <div className='flex flex-col gap-3 pt-3 md:flex-row md:pt-6'>
        <div className='w-full md:w-1/2'>
          {/* Step 1 */}
          {step === 1 && (
            <CheckoutForm
              className='flex flex-col gap-3 rounded-2xl border-2 border-carbon-300 p-3 dark:border-carbon-800 md:p-6'
              submitCheckoutForm={handleCheckoutForm}
              server_locations={props.product.attributes.server_locations.map(
                (el) => el.name
              )}
            />
          )}

          {/* Step 2 */}
          {step === 2 && props.paypalClientId && formData && (
            <PaypalBtn
              intentType='subscription'
              clientId={props.paypalClientId}
              prices={props.product.attributes.prices}
              successPageSlug={
                `/checkout/${props.product.attributes.success_page?.data.attributes.slug}` ||
                '/'
              }
              formData={formData}
            />
          )}
        </div>

        <div className='flex w-full flex-col gap-3 rounded-2xl border-2 border-carbon-300 p-3 dark:border-carbon-800 md:w-1/2 md:p-6'>
          <div className='flex w-full items-center justify-between text-lg md:text-xl'>
            <p className='text-carbon-700 dark:text-carbon-400'>
              {translations.checkout?.total_amount}
            </p>
            <p className='font-medium'>
              <FormatPrice
                text={translations.checkout?.format_price}
                prices={props.product.attributes.prices}
              />
            </p>
          </div>
          <hr className='border-t-2 border-carbon-300 dark:border-carbon-800' />
          <PriceDetails
            className='flex w-full items-center justify-between'
            lineClassName='text-carbon-700 dark:text-carbon-400'
            amountClassName='font-medium'
            prices={props.product.attributes.prices}
          />
        </div>
      </div>

      <div className='flex justify-between pt-3 md:pt-6'>
        {step === 1 && (
          <PreviousLink
            className='inline-flex cursor-pointer items-center text-carbon-700 dark:text-carbon-400'
            dismissBack={true}
          >
            {translations.btn?.previous}
          </PreviousLink>
        )}
        {step === 2 && (
          <PreviousLink
            className='inline-flex cursor-pointer items-center text-carbon-700 dark:text-carbon-400'
            dismissBack={false}
            dismissAction={() => setStep(1)}
          >
            {translations.btn?.previous}
          </PreviousLink>
        )}
        {step === 1 && (
          <Button type='submit' form='checkout-form'>
            {translations.btn?.submit}
          </Button>
        )}
      </div>
    </>
  );
};

export default CheckoutSteps;
