import Image from 'next/image';
import { MDXRemote } from 'next-mdx-remote/rsc';

import { QueryMenus } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';

import Link from '@/components/elements/links';

import { useServer } from '@/store/serverStore';

const Footer = async () => {
  const locale = useServer.getState().locale;

  const { data } = await QueryMenus(locale);
  const { footer } = data.attributes;

  return (
    <footer className='relative w-full overflow-hidden border-t-2 border-carbon-900 bg-carbon-200 font-normal text-carbon-900 dark:border-white dark:bg-carbon-900 dark:font-light dark:text-white'>
      <span
        className='absolute bottom-0 right-0 h-[1500px] w-[1500px] translate-x-1/2 translate-y-1/2 bg-contain bg-center bg-no-repeat'
        style={{ backgroundImage: 'url(/images/rond-orange.avif)' }}
      ></span>
      <div className='relative w-full bg-carbon-200/40 dark:bg-carbon-900/70'>
        <div className='layout flex flex-col gap-6 p-3 py-6 md:px-6 lg:px-12'>
          <div className='grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5'>
            {footer.columns.map((column) => (
              <nav
                key={column.id}
                className='flex flex-col gap-3 text-center md:text-left'
              >
                {!column.logo.data && column.title && (
                  <h3 className='text-base font-semibold text-carbon-700 dark:text-carbon-400 xl:text-lg'>
                    {column.title}
                  </h3>
                )}
                {column.logo.data && (
                  <Image
                    src={MediaUrl(column.logo.data.attributes.url)}
                    priority
                    quality={100}
                    width={column.logo.data.attributes.width}
                    height={column.logo.data.attributes.height}
                    alt={column.logo.data.attributes.alternativeText ?? ''}
                    className='h-10 w-full object-contain object-center md:object-left'
                    sizes='50vw'
                  />
                )}
                {column.description && (
                  <p className='text-sm xl:text-base'>{column.description}</p>
                )}
                {column.socials.length > 0 && (
                  <div className='flex flex-row justify-center gap-3 md:justify-start'>
                    {column.socials.map((social) => (
                      <Link
                        key={social.id}
                        title={social.name}
                        href={includeLocaleLink(social.href)}
                        icon={social.icon}
                        openNewTab={social.open_new_tab}
                        style={social.style}
                        variant={social.variant}
                        rel={social.relationship}
                        aria-label={social.name}
                        className='text-xl text-carbon-900 hover:text-primary-600 focus:text-primary-600 active:text-primary-600 dark:text-white lg:text-2xl xl:text-3xl'
                      >
                        {social.name}
                      </Link>
                    ))}
                  </div>
                )}
                {column.links.length > 0 && (
                  <ul className='flex flex-col gap-3 text-sm underline xl:text-base'>
                    {column.links.map((item) => (
                      <li key={item.id}>
                        <Link
                          title={item.name}
                          href={includeLocaleLink(item.href)}
                          icon={item.icon}
                          openNewTab={item.open_new_tab}
                          style={item.style}
                          variant={item.variant}
                          rel={item.relationship}
                          aria-label={item.name}
                        >
                          {item.name}
                        </Link>
                      </li>
                    ))}
                  </ul>
                )}
              </nav>
            ))}
          </div>
          <div className='flex flex-col gap-3 text-center text-sm text-carbon-700 dark:text-carbon-400 xl:text-base'>
            <MDXRemote source={footer.copyright} />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
