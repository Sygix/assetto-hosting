import { create } from 'zustand';

import { gtag } from '@/lib/helper';
import { CookiesSetting } from '@/lib/interfaces';

export interface CookieState {
  cookiesSetting: CookiesSetting;
  consent: Record<string, string>;
}

export interface CookieActions {
  setConsent: (consent: Record<string, string>, eventType?: string) => void;
}

export const useCookieServer = create<CookieState>(() => ({
  cookiesSetting: {
    cookies: [],
  },
  consent: {},
}));

export const useCookie = create<CookieState & CookieActions>((set) => ({
  cookiesSetting: {
    cookies: [],
  },
  consent: {},
  setConsent: (consent, eventType = 'default') =>
    set(() => {
      // Update local storage
      localStorage.setItem('consent', JSON.stringify(consent));

      // Update Google Analytics
      gtag('consent', eventType, consent);

      // Update store
      return {
        consent: consent,
      };
    }),
}));
