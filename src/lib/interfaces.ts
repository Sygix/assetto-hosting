import { HeroImageType } from "@/components/sections/HeroImage/types";

export interface CartItem {
  product: Product;
  size: string;
  price: number;
  qty: number;
  color?: string;
}

export interface Product {
  id: number;
  attributes: {
    title: string;
    slug: string;
    description: string;
    short_description?: string;
    checkout_text?: string;
    server_locations: {
      name: string;
    }[];
    medias: {
      data: UploadFile[];
    };
    categories?: {
      data: Category[];
    };
    prices: ProductPrice[];
    success_page?: {
      data: Page;
    };
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations?: {
      data: Localizations[];
    };
  };
}

export interface Category {
  id: number;
  attributes: {
    title: string;
    slug: string;
    description: string;
    short_description: string;
    price_text: string;
    btn_text: string;
    products: {
      data: Product[];
    };
    content: {
      __typename: string;
    };
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations?: {
      data: Localizations[];
    };
  };
}

export interface Page {
  id: number;
  attributes: {
    title?: string;
    slug: string;
    content: [
      {
        __typename: string;
      }
    ];
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations?: {
      data: Localizations[];
    };
  };
}

export interface Landing {
  id: number;
  attributes: {
    title: string;
    slug: string;
    description: string;
    metadata: SeoMetadata;
    content: [
      {
        __typename: string;
      }
    ];
    heroImage?: HeroImageType;
    locale: string;
    updatedAt: string;
    background_color?: string;
    landing_footer?: string;
    localizations?: {
      data: Localizations[];
    };
  };
}
export interface Article {
  id: number;
  attributes: {
    title: string;
    slug: string;
    short_description: string;
    thumbnail: {
      data: UploadFile;
    };
    cover: {
      data?: UploadFile;
    };
    content: string;
    author: string;
    article_tags: ArticleTag[];
    metadata: SeoMetadata;
    locale: string;
    updatedAt?: string;
    publishedAt?: string;
    localizations?: {
      data: Localizations[];
    };
  };
}

export interface Referral {
  id: number;
  attributes: {
    tag: string;
    hits: number;
  };
}

export interface ArticleTag {
  id: number;
  attributes: {
    name: string;
    articles: {
      data: Article[];
    };
  };
}

export interface Setting {
  attributes: {
    favicons: {
      data: {
        attributes: {
          url: string;
        };
      }[];
    };
    seo: DefaultSeoMetadata;
    payment_provider: string;
    mapbox_public_key?: string;
    paypal_client_id?: string;
    google_tag_id?: string;
    default_currency: string;
    currencies: string[];
    provide_support_script?: string;
  };
}

export interface CookiesSetting {
  cookies: {
    name: string;
    description: string;
    mandatory: boolean;
    default: string;
  }[];
}

export interface Menu {
  attributes: {
    header: Header;
    footer: Footer;
  };
}

export interface Header {
  id: number;
  logo: {
    data: UploadFile;
  };
  logo_link: string;
  items: HeaderItem[];
}

export interface HeaderItem {
  id: number;
  link: LinkInterface;
  sublinks: LinkInterface[];
}

export interface Footer {
  id: number;
  columns: FooterColumn[];
  copyright: string;
}

export interface FooterColumn {
  id: number;
  logo: {
    data?: UploadFile;
  };
  title: string;
  description?: string;
  socials: LinkInterface[];
  links: LinkInterface[];
}

export interface LinkInterface {
  id: number;
  name: string;
  href: string;
  open_new_tab: boolean;
  icon?: string;
  style: ENUM_ELEMENTS_LINK_STYLE;
  direction: ENUM_ELEMENTS_LINK_DIRECTION;
  variant: ENUM_ELEMENTS_LINK_VARIANT;
  relationship?: string;
}

export interface Order {
  id: number;
  attributes: {
    subscription_id: string;
    status: ENUM_ORDER_STATUS;
    status_update_time?: string;
    product: {
      data?: Product;
    };
    start_time?: string;
    create_time?: string;
    update_time?: string;
    email_address?: string;
    subscriber_name?: string;
    subscriber_country_code?: string;
    failed_payments_count: number;
    next_billing_time?: string;
    additional_infos?: string;
    order_id: string;
    createdAt?: string;
    updatedAt?: string;
  };
}

export interface ProductPrice {
  currency: string;
  price: number;
  sale_price?: number;
  on_sale_from?: string;
  on_sale_to?: string;
  currency_symbol: string;
  details: {
    line_name: string;
    amount: number;
  }[];
  paypal_plan_id?: string;
}

export interface OrderProducts {
  id: number;
  title: string;
  price: number;
  qty: number;
  size: string;
  sizeId: number;
  color?: string;
}

export type localeProps = {
  i18NLocales: {
    data: [
      {
        attributes: {
          code: string;
          name: string;
        };
      }
    ];
  };
};

export interface UploadFile {
  id: number;
  attributes: {
    alternativeText?: string;
    caption?: string;
    name?: string;
    url: string;
    width: number;
    height: number;
    mime: string;
  };
}

export interface Media {
  id: number;
  attributes: {
    name: string;
    slug: string;
    media: {
      data?: UploadFile;
    };
    thumbnail: {
      data?: UploadFile;
    };
    ext_video?: {
      provider: string;
      providerUid: string;
      url: string;
    };
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations: {
      data: Localizations[];
    };
  };
}

export interface QueryMetaProps {
  pagination: {
    page: number;
    pageCount: number;
  };
}

interface DefaultSeoMetadata {
  title?: string;
  siteName?: string;
  description?: string;
}

interface SeoMetadata {
  template_title?: string;
  title_suffix?: string;
  meta_description?: string;
}

export interface Localizations {
  attributes: {
    locale: string;
    slug: string;
  };
}

export enum ENUM_ORDER_STATUS {
  CHECKOUT = 'CHECKOUT',
  APPROVAL_PENDING = 'APPROVAL_PENDING',
  APPROVED = 'APPROVED',
  ACTIVE = 'ACTIVE',
  SUSPENDED = 'SUSPENDED',
  CANCELLED = 'CANCELLED',
  EXPIRED = 'EXPIRED',
}

export enum PAYMENT_PROVIDER {
  STRIPE = 'STRIPE',
}

export enum ENUM_ELEMENTS_LINK_STYLE {
  primary = 'primary',
  underline = 'underline',
  button = 'button',
  icon = 'icon',
  arrow = 'arrow',
  none = 'none',
}

export enum ENUM_ELEMENTS_LINK_DIRECTION {
  left = 'left',
  right = 'right',
}

export enum ENUM_ELEMENTS_LINK_VARIANT {
  primary = 'primary',
  outline = 'outline',
  ghost = 'ghost',
  light = 'light',
  dark = 'dark',
}

export enum ENUM_MAX_WIDTH_SCREEN {
  'default' = '',
  'none' = 'max-w-none',
  'max_w_screen_sm' = 'max-w-screen-sm',
  'max_w_screen_md' = 'max-w-screen-md',
  'max_w_screen_lg' = 'max-w-screen-lg',
  'max_w_screen_xl' = 'max-w-screen-xl',
  'max_w_screen_2xl' = 'max-w-screen-2xl',
}

export interface Redirection {
  attributes: {
    newPath: string;
    oldPath: string;
    type: REDIRECTIONS;
  };
}

export enum REDIRECTIONS {
  'permanent_redirect' = 'permanent_redirect',
  'temporary_redirect' = 'temporary_redirect',
  'rewrite' = 'rewrite',
}
