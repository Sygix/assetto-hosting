'use server';

import { MutationCreateOrder, QueryReferralByTag } from '@/lib/graphql';

type StrapiError = {
  error?: {
    type: string;
    message?: string;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
};

const createOrder = async (props: {
  order_id: string;
  payment_source: string;
  subscription_id: string;
  failed_payments_count: number;
  delivery_email_address: string;
  server_location: string;
  referral_tag?: string | null;
}): Promise<StrapiError> => {
  let referral_id: number | undefined;

  if (props.referral_tag) {
    try {
      const res = await QueryReferralByTag(props.referral_tag);
      referral_id = res.referrals.data[0].id;
    } catch (_) {
      //nothing
    }
  }

  const input = {
    order_id: props.order_id,
    payment_source: props.payment_source,
    subscription_id: props.subscription_id,
    failed_payments_count: props.failed_payments_count,
    delivery_email_address: props.delivery_email_address,
    server_location: props.server_location,
    referral: referral_id,
  };

  try {
    if (props) {
      const { createOrder } = await MutationCreateOrder(input);
      return { data: { order_id: createOrder.data.id } };
    }
    return { error: { type: 'invalid-input' } };
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export default createOrder;
